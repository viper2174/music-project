package com.practice.music.controllers;

import com.practice.music.entities.Album;
import com.practice.music.entities.Singer;
import com.practice.music.entities.Song;
import com.practice.music.services.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class MainPageControllerTest {

    private static final String UNIQUE_PART_OF_MAIN_PAGE = "<title>Music Application main page</title>";
    private static final String UNIQUE_PART_OF_SINGER_PAGE = "<title>Singer page</title>";
    private static final String UNIQUE_PART_OF_ALBUM_PAGE = "<title>Album page</title>";
    private static final String UNIQUE_PART_OF_SONG_PAGE = "<title>Song page</title>";
    private static final String TEST_STRING = "test";
    private static final String TEST_INT_AS_STRING = "1";
    private static final int TEST_INT = 1;

    @InjectMocks
    private MainPageController mainPageController;

    @Autowired
    WebApplicationContext wac;

    @MockBean
    private GenreService genreService;

    @MockBean
    private AlbumService albumService;

    @MockBean
    private SingerService singerService;

    @MockBean
    private SongService songService;

    @MockBean
    private UserService userService;

    @Autowired
    private MockMvc mockMvc;

    @BeforeEach
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }

    @Test
    void shouldReturnMainPageWhenTryToGetMainPage() throws Exception {
        Singer singer = new Singer();
        Album album = new Album();
        album.setName("test");
        Song song = new Song();
        song.setAlbum(album);
        album.setSinger(singer);
        List<Song> songList = new ArrayList<>();
        songList.add(song);
        Mockito.when(songService.findPaginated(any(Pageable.class)))
                .thenReturn(new PageImpl<Song>(songList,PageRequest.of(1,1),0));
        this.mockMvc.perform(get("/"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString(UNIQUE_PART_OF_MAIN_PAGE)))
                .andExpect(model().attributeExists("songPage", "pageNumbers", "foundSongs"));
    }

    @Test
    void shouldReturnSingerPageWhenTryToGetSingerPage() throws Exception {
        Singer singer = new Singer();
        Optional<Singer> optionalSinger = Optional.of(singer);
        Mockito.when(singerService.findById(1)).thenReturn(optionalSinger);
        this.mockMvc.perform(get("/singer")
                .param("id", "1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString(UNIQUE_PART_OF_SINGER_PAGE)))
                .andExpect(model().attributeExists("singer", "albumList"));
    }

    @Test
    void shouldReturnMainPageWithAllSongsPaginatedWhenTryToGetSearchResults() throws Exception {
        List<Song> testSongList = new ArrayList<>();
        Page<Song> testSongPage = new PageImpl<>(testSongList);
        Mockito.when(songService.findPaginated(any(Pageable.class))).thenReturn(testSongPage);
        this.mockMvc.perform(get("/search")
                .param("page", TEST_INT_AS_STRING)
                .param("size", TEST_INT_AS_STRING))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString(UNIQUE_PART_OF_MAIN_PAGE)))
                .andExpect(model().attributeExists
                        ("q", "songPage", "foundSongs", "pageNumbers"));
    }

    @Test
    void shouldReturnMainPageWithSearchDataWhenTryToGetSearchResults() throws Exception {
        List<Song> testSongList = new ArrayList<>();
        List<Singer> testSingerList = new ArrayList<>();
        List<Album> testAlbumList = new ArrayList<>();
        Page<Song> testSongPage = new PageImpl<>(testSongList);
        Mockito.when(songService.findByNameContains(TEST_STRING)).thenReturn(testSongList);
        Mockito.when(songService.findByNameContains(TEST_STRING, PageRequest.of(TEST_INT - 1, TEST_INT)))
                .thenReturn(testSongPage);
        Mockito.when(singerService.findByNameContains(TEST_STRING)).thenReturn(testSingerList);
        Mockito.when(albumService.findByNameContains(TEST_STRING)).thenReturn(testAlbumList);
        this.mockMvc.perform(get("/search")
                .param("q", TEST_STRING)
                .param("page", TEST_INT_AS_STRING)
                .param("size", TEST_INT_AS_STRING))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString(UNIQUE_PART_OF_MAIN_PAGE)))
                .andExpect(model().attributeExists
                        ("q", "pageNumbers", "foundSingers", "foundSongs","foundAlbums"));
    }

    @Test
    void shouldReturnSingerPageWithoutModelAttributesWhenTryToGetSingerPageWithUnrealId() throws Exception {
        Optional<Singer> optionalSinger = Optional.empty();
        Mockito.when(singerService.findById(100)).thenReturn(optionalSinger);
        this.mockMvc.perform(get("/singer")
                .param("id", "100"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString(UNIQUE_PART_OF_SINGER_PAGE)))
                .andExpect(model().attributeDoesNotExist("singer", "albumList"));
    }

    @Test
    void shouldReturnAlbumPageWhenTryToGetAlbumPage() throws Exception {
        Album album = new Album();
        Optional<Album> optionalAlbum = Optional.of(album);
        Mockito.when(albumService.findById(1)).thenReturn(optionalAlbum);
        this.mockMvc.perform(get("/album")
                .param("id", "1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString(UNIQUE_PART_OF_ALBUM_PAGE)))
                .andExpect(model().attributeExists("album", "songList"));
    }

    @Test
    void shouldReturnAlbumPageWithoutModelAttributesWhenTryToGetAlbumPageWithUnrealId() throws Exception {
        Optional<Album> optionalAlbum = Optional.empty();
        Mockito.when(albumService.findById(100)).thenReturn(optionalAlbum);
        this.mockMvc.perform(get("/album")
                .param("id", "100"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString(UNIQUE_PART_OF_ALBUM_PAGE)))
                .andExpect(model().attributeDoesNotExist("singer", "albumList"));
    }

    @Test
    void shouldReturnSongPageWhenTryToGetSongPage() throws Exception {
        Song song = new Song();
        Optional<Song> optionalSong = Optional.of(song);
        Mockito.when(songService.findById(1)).thenReturn(optionalSong);
        this.mockMvc.perform(get("/song")
                .param("id", "1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString(UNIQUE_PART_OF_SONG_PAGE)))
                .andExpect(model().attributeExists("song"));
    }

    @Test
    void shouldReturnSongPageWithoutModelAttributesWhenTryToGetSongPageWithUnrealId() throws Exception {
        Optional<Song> optionalSong = Optional.empty();
        Mockito.when(songService.findById(100)).thenReturn(optionalSong);
        this.mockMvc.perform(get("/song")
                .param("id", "100"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString(UNIQUE_PART_OF_SONG_PAGE)))
                .andExpect(model().attributeDoesNotExist("song"));
    }

}