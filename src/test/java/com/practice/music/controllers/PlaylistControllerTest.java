package com.practice.music.controllers;

import com.google.gson.Gson;
import com.practice.music.entities.Playlist;
import com.practice.music.entities.Song;
import com.practice.music.entities.User;
import com.practice.music.models.JsonPlaylistResponse;
import com.practice.music.services.PlaylistService;
import com.practice.music.services.SongService;
import com.practice.music.services.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Optional;

import static org.hamcrest.Matchers.containsString;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class PlaylistControllerTest {

    private static final String UNIQUE_PART_OF_PLAYLIST_PAGE = "<title>Playlist page</title>";
    private static final String PLAYLIST_WITH_SUCH_ID_DOES_NOT_EXIST = "playlist with such id does not exist";
    private static final int TEST_ID = 1;
    private static final String TEST_NAME = "name";
    private static final String UNIQUE_PART_OF_ERROR_PAGE = "<title>Error page</title>";
    private static final String USER_NOT_AUTHORIZED = "user is not authorized. Authorize pls to use playlist feature";
    private static final String FAILURE = "failure";
    private static final String SONG_WITH_ID_NOT_EXIST = "song with such id does not exist";
    private static final int EMPTY_ID = 0;
    @InjectMocks
    private PlaylistController playlistController;

    @Autowired
    WebApplicationContext wac;

    @MockBean
    private PlaylistService playlistService;

    @MockBean
    private SongService songService;

    @MockBean
    private UserService userService;

    @Autowired
    private MockMvc mockMvc;

    @BeforeEach
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }

    @Test
    void shouldReturnPlaylistPageWhenTryToGetPlaylistPage() throws Exception {
        Playlist testPlaylist = new Playlist();
        Mockito.when(playlistService.findById(1)).thenReturn(Optional.of(testPlaylist));
        this.mockMvc.perform(get("/playlist")
                .param("id", "1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString(UNIQUE_PART_OF_PLAYLIST_PAGE)))
                .andExpect(model().attributeExists("playlist"));
        Mockito.verify(playlistService).findById(1);
    }

    @Test
    void shouldReturnErrorPageWhenTryToGetPlaylistPage() throws Exception {
        Mockito.when(playlistService.findById(TEST_ID)).thenReturn(Optional.empty());
        this.mockMvc.perform(get("/playlist")
                .param("id", "1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString(UNIQUE_PART_OF_ERROR_PAGE)))
                .andExpect(model().attributeExists("error"));
    }

    @Test
    void shouldReturn200StatusWhenTryToRemovePlaylistWithProperId() throws Exception{
        User user = new User();
        user.setLogin("test");
        Mockito.doNothing().when(playlistService).deleteById(TEST_ID, user);
        Mockito.when(userService.findByLogin(anyString())).thenReturn(Optional.empty());
        this.mockMvc.perform(post("/removePlaylist")
                .param("id", "1")
                .sessionAttr("user", user))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    void shouldReturnBadRequestStatusWhenTryToRemovePlaylistWithNonexistentId() throws Exception{
        this.mockMvc.perform(post("/removePlaylist")
                .param("id", "bad data"))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    void shouldReturnFailureWithMessageWhenTryToAddNewPlaylistWithNonexistentSongId () throws Exception {
        Mockito.when(songService.findById(TEST_ID)).thenReturn(Optional.empty());
        MockHttpServletResponse response = this.mockMvc.perform(get("/addInPlaylist")
                .param("playlistId", "1")
                .param("songId", "1").sessionAttr("user", new User()))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse();
        assertEquals(response.getContentAsString(),
                new Gson().toJson(new JsonPlaylistResponse(FAILURE, SONG_WITH_ID_NOT_EXIST, EMPTY_ID)));
    }

    @Test
    void shouldReturnErrorWhenTryToAddSongToPlaylistWithoutUserHasBeenAuthorized () throws Exception {
        Mockito.when(songService.findById(TEST_ID)).thenReturn(Optional.of(new Song()));
        MockHttpServletResponse response = this.mockMvc.perform(get("/addInPlaylist")
                .param("playlistId", "1")
                .param("songId", "1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse();
        assertEquals(response.getContentAsString(),
                new Gson().toJson(new JsonPlaylistResponse(FAILURE, USER_NOT_AUTHORIZED, EMPTY_ID)));
    }

    @Test
    void shouldReturnCustomJsonWhenTryToAddExistedSongToExistedPlaylistWithUserHasBeenAuthorized () throws Exception {
        Song song = new Song();
        User user = new User();
        JsonPlaylistResponse expected = new JsonPlaylistResponse(TEST_NAME, TEST_NAME, TEST_ID);
        Mockito.when(songService.findById(TEST_ID)).thenReturn(Optional.of(song));
        Mockito.when(playlistService.saveSongInPlaylistAndObtainJson(song,user, TEST_ID)).thenReturn(expected);
        MockHttpServletResponse response = this.mockMvc.perform(get("/addInPlaylist")
                .param("playlistId", "1")
                .param("songId", "1").sessionAttr("user", user))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse();
        assertEquals(response.getContentAsString(), new Gson().toJson(expected));
    }

    @Test
    void shouldRedirectOnPreviousPageWhenTryToEditPlaylistWithNewName() throws Exception {
        Mockito.when(playlistService.editPlaylist(TEST_ID, "newName")).thenReturn(true);
        this.mockMvc.perform(post("/editPlaylist")
                .header("referer", "https:localhost:8080/")
                .param("playlistId", "1")
                .param("newName", "newName"))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("localhost:8080/"))
                .andReturn();
    }

    @Test
    void shouldRedirectOnPreviousPageWhenTryToEditPlaylistWithNewNameAndNonexistentId() throws Exception {
        this.mockMvc.perform(post("/editPlaylist")
                .header("referer", "https:localhost:8080/")
                .param("playlistId", "wrongValue")
                .param("newName", "newName"))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("localhost:8080/"))
                .andReturn();
        Mockito.verify(playlistService, Mockito.never()).editPlaylist(anyInt(), anyString());
    }
}