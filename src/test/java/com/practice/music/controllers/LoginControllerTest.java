package com.practice.music.controllers;

import com.practice.music.entities.User;
import com.practice.music.services.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Optional;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@SpringBootTest
@AutoConfigureMockMvc
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class LoginControllerTest {

    private static final String UNIQUE_PART_OF_LOGIN_PAGE = "<input type=\"submit\" value=\"Log In\"/>";
    private static final String ADMIN_STRING = "admin";
    private static final String USER_STRING = "user";
    private static final String WRONG_DATA = "wrong";

    @InjectMocks
    private LoginController loginController;

    @Autowired
    WebApplicationContext wac;

    @MockBean
    private UserService userService;

    @Autowired
    private MockMvc mockMvc;

    @BeforeEach
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }

    @Test
    void shouldReturnRegisterPageWhenTryTogetRegisterPage() throws Exception {
        this.mockMvc.perform(get("/login"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString(UNIQUE_PART_OF_LOGIN_PAGE)))
                .andExpect(model().attributeExists("loginForm"));
    }

    @Test
    void shouldReturnAdminPageWhenTryToLoginLikeAdmin() throws Exception {
        User testUser = new User();
        testUser.setLogin(ADMIN_STRING);
        testUser.setPassword(ADMIN_STRING);
        testUser.setRole("admin");
        Optional<User> testUserOptional = Optional.ofNullable(testUser);
        when(userService.findByLogin(any(String.class))).thenReturn(testUserOptional);
        this.mockMvc.perform(post("/login/check")
                .param("login", ADMIN_STRING)
                .param("password", ADMIN_STRING))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/admin"))
                .andExpect(model().hasNoErrors());

    }

    @Test
    void shouldReturnProfilePageWhenTryToLoginLikeUser() throws Exception {
        User testUser = new User();
        testUser.setLogin(USER_STRING);
        testUser.setPassword(USER_STRING);
        testUser.setRole(USER_STRING);
        Optional<User> testUserOptional = Optional.ofNullable(testUser);
        when(userService.findByLogin(any(String.class))).thenReturn(testUserOptional);
        this.mockMvc.perform(post("/login/check")
                .param("login", USER_STRING)
                .param("password", USER_STRING))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/profile"))
                .andExpect(model().hasNoErrors());

    }

    @Test
    void shouldReturnLoginPageWhenTryToLoginWithNonExistingUser() throws Exception {
        Optional<User> testUserOptional = Optional.empty();
        when(userService.findByLogin(any(String.class))).thenReturn(testUserOptional);
        this.mockMvc.perform(post("/login/check")
                .param("login", USER_STRING)
                .param("password", USER_STRING))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().string(containsString(UNIQUE_PART_OF_LOGIN_PAGE)))
                .andExpect(model().hasNoErrors());

    }

    @Test
    void shouldReturnLoginPageWhenTryToLoginWithEmptyData() throws Exception {
        String [] errorFields = {"login","password"};
        this.mockMvc.perform(post("/login/check")
                .param("login", "")
                .param("password", ""))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().string(containsString(UNIQUE_PART_OF_LOGIN_PAGE)))
                .andExpect(model().hasErrors())
                .andExpect(model().attributeErrorCount("loginForm", 3))
                .andExpect(model().attributeHasFieldErrors("loginForm", errorFields));

    }

    @Test
    void shouldReturnLoginPageWhenTryToLoginWithInvalidData() throws Exception {
        String [] errorFields = {"login","password"};
        this.mockMvc.perform(post("/login/check")
                .param("login", WRONG_DATA)
                .param("password", WRONG_DATA))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().string(containsString(UNIQUE_PART_OF_LOGIN_PAGE)))
                .andExpect(model().hasErrors())
                .andExpect(model().attributeErrorCount("loginForm", 1));

    }

}