package com.practice.music.controllers;

import com.google.gson.Gson;
import com.practice.music.entities.Album;
import com.practice.music.entities.Genre;
import com.practice.music.entities.Singer;
import com.practice.music.entities.Song;
import com.practice.music.models.JsonAlbumInfo;
import com.practice.music.services.AlbumService;
import com.practice.music.services.GenreService;
import com.practice.music.services.SingerService;
import com.practice.music.services.SongService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class AdminControllerTest {

    private static final String UNIQUE_PART_OF_ADMIN_PAGE = "<title>Music Application admin page</title>";
    private static final String UNIQUE_PART_OF_ADD_MUSICIAN_PAGE = "<title>Add musician admin page</title>";
    private static final String UNIQUE_PART_OF_ADD_ALBUM_PAGE = "<title>Add album admin page</title>";
    private static final String UNIQUE_PART_OF_ADD_SONG_PAGE = "<title>Add song admin page</title>";
    private static final String UNIQUE_PART_OF_ADD_GENRE_PAGE = "<title>Add genre admin page</title>";
    private static final String TEST_STRING = "test";
    private static final String EMPTY_STRING = "";
    private static final String TEST_INT = "100";
    private static final String CORRECT_DATA = "1";
    private static final String TOO_LONG_LINK = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" +
            "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" +
            "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" +
            "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" +
            "aaaaaaaaaaaaaaaa";

    @InjectMocks
    private MainPageController mainPageController;

    @Autowired
    WebApplicationContext wac;

    @MockBean
    private GenreService genreService;

    @MockBean
    private AlbumService albumService;

    @MockBean
    private SingerService singerService;

    @MockBean
    private SongService songService;

    @Autowired
    private MockMvc mockMvc;

    @BeforeEach
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }

    @Test
    void shouldReturnAdminPageWhenTryToGetAdminPage() throws Exception {
        this.mockMvc.perform(get("/admin"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString(UNIQUE_PART_OF_ADMIN_PAGE)));
    }

    @Test
    void shouldReturnAddAlbumPageWhenTryToGetAddAlbumPage() throws Exception {
        this.mockMvc.perform(get("/admin/addAlbumPage"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString(UNIQUE_PART_OF_ADD_ALBUM_PAGE)))
                .andExpect(model().attributeExists("musicians", "albums", "albumForm"));
    }

    @Test
    void shouldReturnAddSongPageWhenTryToGetAddSongPage() throws Exception {
        this.mockMvc.perform(get("/admin/addSongPage"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString(UNIQUE_PART_OF_ADD_SONG_PAGE)))
                .andExpect(model().attributeExists("musicians", "songs", "albums", "songForm"));
    }

    @Test
    void shouldReturnAddGenrePageWhenTryToGetAddGenrePage() throws Exception {
        this.mockMvc.perform(get("/admin/addGenrePage"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString(UNIQUE_PART_OF_ADD_GENRE_PAGE)))
                .andExpect(model().attributeExists("genres", "genreForm"));
    }

    @Test
    void shouldReturnAddMusicianPageWhenTryToGetAddMusicianPage() throws Exception {
        this.mockMvc.perform(get("/admin/addMusicianPage"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString(UNIQUE_PART_OF_ADD_MUSICIAN_PAGE)))
                .andExpect(model().attributeExists("musicians", "genreList", "singerForm"));
    }

    @Test
    void shouldDoNothingWhenTryToRemoveMusicianByValidId() throws Exception {
        Mockito.doNothing().when(singerService).deleteById(Integer.valueOf("100"));
        this.mockMvc.perform(post("/admin/removeMusician/100"))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/admin/addMusicianPage"))
                .andExpect(model().attributeExists("musicians"));
    }

    @Test
    void shouldReturnSameWhenTryToRemoveMusicianByInvalidId() throws Exception {
        this.mockMvc.perform(post("/admin/removeMusician/wrongVar"))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().string(containsString(UNIQUE_PART_OF_ADD_MUSICIAN_PAGE)))
                .andExpect(model().attributeExists("error", "songs", "musicians", "albums", "singerForm"));
    }

    @Test
    void shouldDoNothingWhenTryToRemoveAlbumByValidId() throws Exception {
        Mockito.doNothing().when(albumService).deleteById(Integer.valueOf("100"));
        this.mockMvc.perform(post("/admin/removeAlbum/100"))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/admin/addAlbumPage"))
                .andExpect(model().attributeExists("albums"));
    }

    @Test
    void shouldReturnSamePageWhenTryToRemoveAlbumByInvalidId() throws Exception {
        this.mockMvc.perform(post("/admin/removeAlbum/wrongVar"))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().string(containsString(UNIQUE_PART_OF_ADD_ALBUM_PAGE)))
                .andExpect(model().attributeExists("error", "musicians", "albums", "albumForm"));
    }

    @Test
    void shouldDoNothingWhenTryToRemoveSongByValidId() throws Exception {
        Mockito.doNothing().when(songService).deleteById(Integer.valueOf("100"));
        this.mockMvc.perform(post("/admin/removeSong/100"))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/admin/addSongPage"))
                .andExpect(model().attributeExists("songs"));
    }

    @Test
    void shouldReturnSamePageWhenTryToRemoveSongByInvalidId() throws Exception {
        this.mockMvc.perform(post("/admin/removeSong/wrongVar"))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().string(containsString(UNIQUE_PART_OF_ADD_SONG_PAGE)))
                .andExpect(model().attributeExists("error", "songs", "musicians", "albums", "songForm"));
    }

    @Test
    void shouldDoNothingWhenTryToRemoveGenreByValidId() throws Exception {
        Mockito.doNothing().when(genreService).deleteById(Integer.valueOf("100"));
        this.mockMvc.perform(post("/admin/removeGenre/100"))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/admin/addGenrePage"))
                .andExpect(model().attributeExists("genres"));
    }

    @Test
    void shouldReturnSamePageWhenTryToRemoveGenreByInvalidId() throws Exception {
        this.mockMvc.perform(post("/admin/removeGenre/wrongVar"))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().string(containsString(UNIQUE_PART_OF_ADD_GENRE_PAGE)))
                .andExpect(model().attributeExists("error", "genres", "genreForm"));
    }

    @Test
    void shouldDoNothingWhenTryToAddNewGenre () throws Exception {
        Mockito.when(genreService.save(any(Genre.class))).thenReturn(new Genre());
        this.mockMvc.perform(post("/admin/addNewGenre")
                .param("name", TEST_STRING))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/admin/addGenrePage"));
    }

    @Test
    void shouldDoNothingWhenTryToAddNewSinger () throws Exception {
        Mockito.when(singerService.save(any(Singer.class))).thenReturn(new Singer());
        Mockito.when(genreService.findAllByIds(any(ArrayList.class))).thenReturn(new ArrayList<Genre>());
        this.mockMvc.perform(post("/admin/addNewSinger")
                .param("name", TEST_STRING)
                .param("logo", TEST_STRING)
                .param("genres", CORRECT_DATA))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/admin/addMusicianPage"));
    }

    @Test
    void shouldReturnAddSingerPageWithErrorWhenTryToAddNewSinger () throws Exception {
        Mockito.when(singerService.save(any(Singer.class))).thenReturn(new Singer());
        Mockito.when(genreService.findAllByIds(any(ArrayList.class))).thenReturn(new ArrayList<Genre>());
        this.mockMvc.perform(post("/admin/addNewSinger")
                .param("name", EMPTY_STRING)
                .param("logo", TOO_LONG_LINK)
                .param("genres", CORRECT_DATA))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(model().errorCount(2));
    }

    @Test
    void shouldDoNothingWhenTryToAddNewAlbum () throws Exception {
        Mockito.when(singerService.findById(Integer.parseInt(CORRECT_DATA))).thenReturn(Optional.of(new Singer()));
        Mockito.when(albumService.save(any(Album.class))).thenReturn(new Album());
        this.mockMvc.perform(post("/admin/addNewAlbum")
                .param("name", TEST_STRING)
                .param("poster", TEST_STRING)
                .param("musicianId", CORRECT_DATA))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/admin/addAlbumPage"));
    }

    @Test
    void shouldSendBackWhenTryToAddNewAlbumWithIncorrectMusicianId () throws Exception {
        Mockito.when(singerService.findById(Integer.parseInt(TEST_INT))).thenReturn(Optional.empty());
        this.mockMvc.perform(post("/admin/addNewAlbum")
                .param("name", TEST_STRING)
                .param("poster", TEST_STRING)
                .param("musicianId", TEST_INT))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().string(containsString(UNIQUE_PART_OF_ADD_ALBUM_PAGE)))
                .andExpect(model().hasErrors());
    }

    @Test
    void shouldDoNothingWhenTryToAddNewSong () throws Exception {
        Mockito.when(albumService.findById(Integer.parseInt(CORRECT_DATA))).thenReturn(Optional.of(new Album()));
        Mockito.when(songService.save(any(Song.class))).thenReturn(new Song());
        this.mockMvc.perform(post("/admin/addNewSong")
                .param("name", TEST_STRING)
                .param("link", TEST_STRING)
                .param("albumId", CORRECT_DATA))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/admin/addSongPage"));
    }

    @Test
    void shouldSendBackWhenTryToAddNewSongWithIncorrectAlbumId () throws Exception {
        Mockito.when(albumService.findById(Integer.parseInt(TEST_INT))).thenReturn(Optional.empty());
        this.mockMvc.perform(post("/admin/addNewSong")
                .param("name", TEST_STRING)
                .param("link", TOO_LONG_LINK)
                .param("albumId", TEST_INT))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().string(containsString(UNIQUE_PART_OF_ADD_SONG_PAGE)))
                .andExpect(model().errorCount(1));
    }

    @Test
    void shouldReturnJsonWhenTryToGetAlbumsByMusicianId () throws Exception {
        List<Album> testAlbumList = new ArrayList<>();
        JsonAlbumInfo jsonAlbumInfo = new JsonAlbumInfo(1, "test");
        List<JsonAlbumInfo> testJsonAlbumInfoList = new ArrayList<>();
        testJsonAlbumInfoList.add(jsonAlbumInfo);
        Mockito.when(singerService.findById(Integer.parseInt(CORRECT_DATA))).thenReturn(Optional.of(new Singer()));
        Mockito.when(albumService.findBySinger(any(Singer.class))).thenReturn(testAlbumList);
        Mockito.when(albumService.getJsonAlbumInfo(testAlbumList)).thenReturn(testJsonAlbumInfoList);
        this.mockMvc.perform(get("/admin/getAlbumsByMusician")
                .param("id", CORRECT_DATA))
                .andDo(print())
                .andExpect(content().string(new Gson().toJson(testJsonAlbumInfoList)));
    }

    @Test
    void shouldReturnNullWhenTryToGetAlbumsByMusicianIdWhichDoesNotExists () throws Exception {
        Mockito.when(singerService.findById(Integer.parseInt(TEST_INT))).thenReturn(Optional.empty());
        this.mockMvc.perform(get("/admin/getAlbumsByMusician")
                .param("id", TEST_INT))
                .andDo(print())
                .andExpect(content().string(EMPTY_STRING));
    }
}