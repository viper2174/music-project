package com.practice.music.controllers;

import com.practice.music.entities.User;
import com.practice.music.models.UserForm;
import com.practice.music.services.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class RegistrationControllerTest {

    private static final String UNIQUE_PART_OF_REGISTRATION_PAGE = "<title>Registration page </title>";
    private static final String TEST_STRING = "name";
    private static final String TEST_STRING_ANOTHER = "name2";

    @InjectMocks
    private RegistrationController registrationController;

    @Autowired
    WebApplicationContext wac;

    @MockBean
    private UserService userService;

    @Autowired
    private MockMvc mockMvc;

    @BeforeEach
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }

    @Test
    void shouldReturnRegisterPageWhenTryTogetRegisterPage() throws Exception {
        this.mockMvc.perform(get("/registration"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString(UNIQUE_PART_OF_REGISTRATION_PAGE)))
                .andExpect(model().attributeExists("userForm"));
    }

    @Test
    void shouldReturnLoginPageWhenTryToRegisterNewUser() throws Exception {
        User testUser = new User();
        when(userService.getUserFromUserForm(any(UserForm.class))).thenReturn(testUser);
        when(userService.save(any(User.class))).thenReturn(testUser);
        this.mockMvc.perform(post("/registration/register")
                .param("login", TEST_STRING)
                .param("password", TEST_STRING)
                .param("passwordConfirm", TEST_STRING))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/login"))
                .andExpect(model().hasNoErrors());

    }

    @Test
    void shouldReturnTwoErrorsWhenTryToRegisterNewUserWithDifferentPasswords() throws Exception {
        User testUser = new User();
        String [] errorFields = {"password","passwordConfirm"};
        when(userService.getUserFromUserForm(any(UserForm.class))).thenReturn(testUser);
        when(userService.save(any(User.class))).thenReturn(testUser);
        this.mockMvc.perform(post("/registration/register")
                .param("login", TEST_STRING)
                .param("password", TEST_STRING)
                .param("passwordConfirm", TEST_STRING_ANOTHER))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(model().attributeErrorCount("userForm", 2))
                .andExpect(model().attributeHasFieldErrors("userForm", errorFields))
                .andExpect(model().hasErrors());

    }

    @Test
    void shouldReturnErrorWhenTryToRegisterNewUserWithEmptyLogin() throws Exception {
        User testUser = new User();
        String errorField = "login";
        when(userService.getUserFromUserForm(any(UserForm.class))).thenReturn(testUser);
        when(userService.save(any(User.class))).thenReturn(testUser);
        this.mockMvc.perform(post("/registration/register")
                .param("login", "")
                .param("password", TEST_STRING)
                .param("passwordConfirm", TEST_STRING))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(model().attributeErrorCount("userForm", 1))
                .andExpect(model().attributeHasFieldErrors("userForm", errorField))
                .andExpect(model().hasErrors());

    }

}