package com.practice.music.services.implementation;

import com.practice.music.entities.Playlist;
import com.practice.music.entities.Song;
import com.practice.music.entities.User;
import com.practice.music.models.JsonPlaylistResponse;
import com.practice.music.repositories.PlaylistRepository;
import com.practice.music.repositories.SongRepository;
import com.practice.music.repositories.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.security.core.parameters.P;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.only;
import static org.mockito.Mockito.times;

public class PlaylistJpaServiceTest {

    private static final Integer TEST_ID_1 = 1;
    private static final String TEST_NAME_1 = "name1";
    private static final Integer NEW_PLAYLIST_VALUE = 0;
    private static final String SUCCESS = "success";
    private static final String ERROR = "error";
    private static final String NEW_PLAYLIST_ADDED_MESSAGE = "new playlist has been added";
    private static final String SONG_ADDED_TO_PLAYLIST_MESSAGE = "song has been added to playlist";
    private static final String PLAYLIST_WITH_ID_NOT_EXIST = "playlist with such id doesn't exist";
    private static final String TEST_NAME_2 = "name2";


    @InjectMocks
    private PlaylistJpaService playlistJpaService;

    @Mock
    private PlaylistRepository playlistRepository;

    @Mock
    private UserRepository userRepository;

    @Mock
    private SongRepository songRepository;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void shouldDoNothingWhenTryToSavePlaylistInDatabase() {
        Playlist testPlaylist = new Playlist();
        Mockito.when(playlistRepository.save(testPlaylist)).thenReturn(testPlaylist);
        assertEquals(playlistJpaService.save(testPlaylist), testPlaylist);
        Mockito.verify(playlistRepository, only()).save(testPlaylist);
    }

    @Test
    void shouldReturnNullWhenTryToFindAllPlaylists() {
        List<Playlist> testPlaylistList = new ArrayList<>();
        Mockito.when(playlistRepository.findAll()).thenReturn(testPlaylistList);
        assertEquals(playlistJpaService.findAll(), testPlaylistList);
        Mockito.verify(playlistRepository).findAll();
    }

    @Test
    void shouldReturnTestPlaylistWhenTryToFindSongById() {
        Optional<Playlist> testPlaylistOptional = Optional.empty();
        Mockito.when(playlistRepository.findById(TEST_ID_1)).thenReturn(testPlaylistOptional);
        assertEquals(testPlaylistOptional, playlistJpaService.findById(TEST_ID_1));
        Mockito.verify(playlistRepository, only()).findById(TEST_ID_1);
    }

    @Test
    void shouldDoNothingWhenTryToDeletePlaylistById() {
        User testUser = new User();
        Mockito.doNothing().when(playlistRepository).deleteById(TEST_ID_1);
        Mockito.when(userRepository.save(any(User.class))).thenReturn(testUser);
        playlistJpaService.deleteById(TEST_ID_1, testUser);
        Mockito.verify(playlistRepository, only()).deleteById(TEST_ID_1);
        Mockito.verify(userRepository, only()).save(any(User.class));
    }

    @Test
    void shouldReturnNewPlaylistJsonInfoWhenTryAddSongToNewPlaylist(){
        int testPlaylistId = NEW_PLAYLIST_VALUE;
        User testUser = new User();
        Song testSong = new Song();
        testSong.setName(TEST_NAME_1);
        testSong.setLink(TEST_NAME_1);
        Playlist testPlaylist = new Playlist();
        testPlaylist.setName(TEST_NAME_1 + " playlist");
        testPlaylist.setId(TEST_ID_1);
        Mockito.when(playlistRepository.save(any(Playlist.class))).thenReturn(testPlaylist);
        JsonPlaylistResponse expected = new JsonPlaylistResponse(SUCCESS, NEW_PLAYLIST_ADDED_MESSAGE, TEST_ID_1);
        assertEquals(expected, playlistJpaService.saveSongInPlaylistAndObtainJson(testSong, testUser, testPlaylistId));
    }

    @Test
    void shouldAddSongToExistedPlaylistWhenTryToAddSongIntoPlaylistWithExistedId(){
        int testPlaylistId = TEST_ID_1;
        User testUser = new User();
        Song testSong = new Song();
        testSong.setName(TEST_NAME_1);
        testSong.setLink(TEST_NAME_1);
        Playlist testPlaylist = new Playlist();
        testPlaylist.setName(TEST_NAME_1 + " playlist");
        testPlaylist.setId(TEST_ID_1);
        Mockito.when(playlistRepository.findById(TEST_ID_1)).thenReturn(Optional.of(testPlaylist));
        Mockito.when(playlistRepository.save(any(Playlist.class))).thenReturn(testPlaylist);
        JsonPlaylistResponse expected = new JsonPlaylistResponse(SUCCESS, SONG_ADDED_TO_PLAYLIST_MESSAGE, TEST_ID_1);
        assertEquals(expected, playlistJpaService.saveSongInPlaylistAndObtainJson(testSong, testUser, testPlaylistId));
    }

    @Test
    void shouldReturnErrorWhenTryToAddSongIntoPlaylistWithNonexistentId(){
        int testPlaylistId = TEST_ID_1;
        User testUser = new User();
        Song testSong = new Song();
        testSong.setName(TEST_NAME_1);
        testSong.setLink(TEST_NAME_1);
        Playlist testPlaylist = new Playlist();
        testPlaylist.setName(TEST_NAME_1 + " playlist");
        testPlaylist.setId(TEST_ID_1);
        Mockito.when(playlistRepository.findById(TEST_ID_1)).thenReturn(Optional.empty());
        Mockito.when(playlistRepository.save(any(Playlist.class))).thenReturn(testPlaylist);
        JsonPlaylistResponse expected = new JsonPlaylistResponse(ERROR, PLAYLIST_WITH_ID_NOT_EXIST, TEST_ID_1);
        assertEquals(expected, playlistJpaService.saveSongInPlaylistAndObtainJson(testSong, testUser, testPlaylistId));
    }

    @Test
    void shouldReturnTrueWhenTryEditPlaylistWithExistingId (){
        Playlist playlistBefore = new Playlist();
        playlistBefore.setId(TEST_ID_1);
        playlistBefore.setName(TEST_NAME_1);
        Playlist playlistAfter = new Playlist();
        playlistAfter.setId(TEST_ID_1);
        playlistAfter.setName(TEST_NAME_2);
        Mockito.when(playlistRepository.findById(TEST_ID_1)).thenReturn(Optional.of(playlistBefore));
        assertTrue(playlistJpaService.editPlaylist(TEST_ID_1, TEST_NAME_2));
    }

    @Test
    void shouldReturnFalseWhenTryEditPlaylistWithNonexistentId(){
        Playlist playlistBefore = new Playlist();
        playlistBefore.setId(TEST_ID_1);
        playlistBefore.setName(TEST_NAME_1);
        Mockito.when(playlistRepository.findById(TEST_ID_1)).thenReturn(Optional.empty());
        assertFalse(playlistJpaService.editPlaylist(TEST_ID_1, TEST_NAME_2));
        Mockito.verify(playlistRepository, Mockito.never()).save(any(Playlist.class));
    }

    @Test
    void shouldRemoveSongWhenTryToRemoveSongFromPlaylist(){
        Playlist playlistBefore = new Playlist();
        playlistBefore.setId(TEST_ID_1);
        List<Song> songs = new ArrayList<>();
        Song testSongToBeRemoved = new Song();
        testSongToBeRemoved.setId(TEST_ID_1);
        songs.add(testSongToBeRemoved);
        playlistBefore.setSongList(songs);
        Optional<Playlist> playlistOptional = Optional.of(playlistBefore);
        Mockito.when(playlistRepository.findById(TEST_ID_1)).thenReturn(playlistOptional);
        Mockito.when(playlistRepository.save(any(Playlist.class))).thenReturn(playlistBefore);
        playlistJpaService.removeSongFromPlaylist(TEST_ID_1, TEST_ID_1);
        Mockito.verify(playlistRepository, times(1)).findById(TEST_ID_1);
        Mockito.verify(playlistRepository, times(1)).save(any(Playlist.class));
    }

    @Test
    void shouldDoNothingWhenTryToRemoveSongFromPlaylistWithNonexistentId(){
        Song testSong = new Song();
        Mockito.when(playlistRepository.findById(TEST_ID_1)).thenReturn(Optional.empty());
        Mockito.when(songRepository.findById(TEST_ID_1)).thenReturn(Optional.of(testSong));
        playlistJpaService.removeSongFromPlaylist(TEST_ID_1, TEST_ID_1);
        Mockito.verify(playlistRepository, times(1)).findById(TEST_ID_1);
        Mockito.verify(songRepository, times(1)).findById(TEST_ID_1);
        Mockito.verify(playlistRepository, Mockito.never()).save(any(Playlist.class));
    }
}