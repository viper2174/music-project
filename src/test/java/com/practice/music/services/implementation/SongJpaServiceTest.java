package com.practice.music.services.implementation;

import com.practice.music.entities.Album;
import com.practice.music.entities.Song;
import com.practice.music.repositories.SongRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

public class SongJpaServiceTest {

    private static final Integer TEST_ID_1 = 1;
    private static final String TEST_NAME_1 = "name1";


    @InjectMocks
    private SongJpaService songJpaService;

    @Mock
    private SongRepository songRepository;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void shouldReturnNullWhenTryToFindAllSongs() {
        Mockito.when(songRepository.findAll()).thenReturn(null);
        assertNull(songJpaService.findAll());
        Mockito.verify(songRepository).findAll();
    }

    @Test
    void shouldDoNothingWhenTryToDeleteSongById() {
        Mockito.doNothing().when(songRepository).deleteById(TEST_ID_1);
        songJpaService.deleteById(TEST_ID_1);
        Mockito.verify(songRepository, Mockito.only()).deleteById(TEST_ID_1);
    }

    @Test
    void shouldDoNothingWhenTryToSaveSongInDatabase() {
        Song testSong = new Song();
        Mockito.when(songRepository.save(testSong)).thenReturn(testSong);
        songJpaService.save(testSong);
        Mockito.verify(songRepository, Mockito.only()).save(testSong);
    }

    @Test
    void shouldReturnTestSongWhenTryTofindSongsByContainingPartOfName() {
        List<Song> testSongList = new ArrayList<>();
        testSongList.add(new Song());
        Mockito.when(songRepository.findByNameContains(TEST_NAME_1)).thenReturn(testSongList);
        assertEquals(testSongList.size(), songJpaService.findByNameContains(TEST_NAME_1).size());
        assertThat(testSongList).containsExactlyElementsOf(songJpaService.findByNameContains(TEST_NAME_1));
        Mockito.verify(songRepository, Mockito.times(2)).findByNameContains(TEST_NAME_1);
    }

    @Test
    void shouldReturnTestSongListWhenTryToFindSongByAlbum() {
        Album testAlbum = new Album();
        List<Song> testSongList = new ArrayList<>();
        testSongList.add(new Song());
        Mockito.when(songRepository.findByAlbum(testAlbum)).thenReturn(testSongList);
        assertEquals(testSongList.size(), songJpaService.findByAlbum(testAlbum).size());
        assertThat(testSongList).containsExactlyElementsOf(songJpaService.findByAlbum(testAlbum));
        Mockito.verify(songRepository, Mockito.times(2)).findByAlbum(testAlbum);
    }

    @Test
    void shouldReturnTestSongWhenTryToFindSongById() {
        Optional<Song> testSongOptinal = Optional.empty();
        Mockito.when(songRepository.findById(TEST_ID_1)).thenReturn(testSongOptinal);
        assertEquals(testSongOptinal, songJpaService.findById(TEST_ID_1));
        Mockito.verify(songRepository, Mockito.only()).findById(TEST_ID_1);
    }

    @Test
    void shouldFindPaginatedPagesWhenTryToObtainSuchDataFromDatabase() {
        Page<Song> testSongPage = new PageImpl<Song>(new ArrayList<>());
        Pageable pageable = PageRequest.of(1,1);
        Mockito.when(songRepository.findAll(pageable)).thenReturn(testSongPage);
        assertEquals(testSongPage, songJpaService.findPaginated(pageable));
        Mockito.verify(songRepository, Mockito.only()).findAll(pageable);
    }

    @Test
    void shouldFindPaginatedPagesByNameContainsWhenTryToObtainSuchDataFromDatabase() {
        Page<Song> testSongPage = new PageImpl<Song>(new ArrayList<>());
        Pageable pageable = PageRequest.of(1,1);
        String testQuery = TEST_NAME_1;
        Mockito.when(songRepository.findByNameContains(testQuery, pageable)).thenReturn(testSongPage);
        assertEquals(testSongPage, songJpaService.findByNameContains(testQuery, pageable));
        Mockito.verify(songRepository, Mockito.only()).findByNameContains(testQuery, pageable);
    }
    @Test
    void shouldTriggerIllegalArgumentExceptionWhenTryToObtainSuchDataFromDatabase() {
        assertThrows(IllegalArgumentException.class, () -> PageRequest.of(1,0));
    }
}