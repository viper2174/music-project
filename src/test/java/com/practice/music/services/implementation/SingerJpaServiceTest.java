package com.practice.music.services.implementation;

import com.practice.music.entities.Singer;
import com.practice.music.repositories.SingerRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class SingerJpaServiceTest {

    private static final Integer TEST_ID_1 = 1;
    private static final String TEST_NAME_1 = "name1";

    @InjectMocks
    private SingerJpaService singerJpaService;

    @Mock
    private SingerRepository singerRepository;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void shouldReturnNullWhenTryToFindAllSingers() {
        Mockito.when(singerRepository.findAll()).thenReturn(null);
        assertNull(singerJpaService.findAll());
        Mockito.verify(singerRepository).findAll();
    }

    @Test
    void shouldDoNothingWhenTryToDeleteSingerById() {
        Mockito.doNothing().when(singerRepository).deleteById(TEST_ID_1);
        singerJpaService.deleteById(TEST_ID_1);
        Mockito.verify(singerRepository, Mockito.only()).deleteById(TEST_ID_1);
    }

    @Test
    void shouldDoNothingWhenTryToSaveSingerInDatabase() {
        Singer testSinger = new Singer();
        Mockito.when(singerRepository.save(testSinger)).thenReturn(testSinger);
        singerJpaService.save(testSinger);
        Mockito.verify(singerRepository, Mockito.only()).save(testSinger);
    }

    @Test
    void shouldReturnTestSingerWhenTryToFindSingerById() {
        Optional<Singer> testSingerOptinal = Optional.empty();
        Mockito.when(singerRepository.findById(TEST_ID_1)).thenReturn(testSingerOptinal);
        assertEquals(testSingerOptinal, singerJpaService.findById(TEST_ID_1));
        Mockito.verify(singerRepository, Mockito.only()).findById(TEST_ID_1);
    }

    @Test
    void shouldReturnTestSingerListWhenTryTofindSingersByContainingPartOfName() {
        List<Singer> testSingerList = new ArrayList<>();
        testSingerList.add(new Singer());
        Mockito.when(singerRepository.findByNameContains(TEST_NAME_1)).thenReturn(testSingerList);
        assertEquals(testSingerList.size(), singerJpaService.findByNameContains(TEST_NAME_1).size());
        assertThat(testSingerList).containsExactlyElementsOf(singerJpaService.findByNameContains(TEST_NAME_1));
        Mockito.verify(singerRepository, Mockito.times(2)).findByNameContains(TEST_NAME_1);
    }
}