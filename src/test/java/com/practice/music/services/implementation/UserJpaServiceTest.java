package com.practice.music.services.implementation;

import com.practice.music.entities.User;
import com.practice.music.models.UserForm;
import com.practice.music.repositories.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

public class UserJpaServiceTest {

    private static final String TEST_NAME_1 = "name1";

    @InjectMocks
    private UserJpaService userJpaService;

    @Mock
    private UserRepository userRepository;

    @Mock
    private PasswordEncoder passwordEncoder;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void shouldReturnTestUserWhenTryToFindUserByLogin() {
        Optional<User> testUserOptinal = Optional.empty();
        when(userRepository.findByLogin(TEST_NAME_1)).thenReturn(testUserOptinal);
        assertEquals(testUserOptinal, userJpaService.findByLogin(TEST_NAME_1));
        Mockito.verify(userRepository, Mockito.only()).findByLogin(TEST_NAME_1);
    }

    @Test
    void shouldReturnUserWhenTryToGetUserFromUserForm() {
        UserForm testUserForm = new UserForm();
        testUserForm.setLogin(TEST_NAME_1);
        testUserForm.setPassword(TEST_NAME_1);
        testUserForm.setPasswordConfirm(TEST_NAME_1);
        User expected = new User();
        expected.setLogin(TEST_NAME_1);
        expected.setPassword(TEST_NAME_1);
        expected.setEnabled(true);
        expected.setRole("user");
        when(passwordEncoder.encode(TEST_NAME_1)).thenReturn(TEST_NAME_1);
        assertEquals(expected, userJpaService.getUserFromUserForm(testUserForm));
        Mockito.verify(passwordEncoder).encode(TEST_NAME_1);
    }

    @Test
    void shouldDoNothingWhenTryToSaveUserInDatabase() {
        User testUser = new User();
        when(userRepository.save(testUser)).thenReturn(testUser);
        userJpaService.save(testUser);
        Mockito.verify(userRepository, Mockito.only()).save(testUser);
    }
}