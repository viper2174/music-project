package com.practice.music.services.implementation;

import com.practice.music.entities.Album;
import com.practice.music.entities.Singer;
import com.practice.music.models.JsonAlbumInfo;
import com.practice.music.repositories.AlbumRepository;
import com.practice.music.services.AlbumService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

public class AlbumJpaServiceTest {

    private static final Integer TEST_ID_1 = 1;
    private static final Integer TEST_ID_2 = 2;
    private static final String TEST_NAME_1 = "name1";
    private static final String TEST_NAME_2 = "name2";


    @InjectMocks
    private AlbumJpaService albumJpaService;

    @Mock
    private AlbumRepository albumRepository;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void shouldReturnListOfJsonAlbumsWhenTryTogetJsonAlbumInfo() {
        AlbumService albumService = new AlbumJpaService();
        Album testAlbum1 = new Album();
        testAlbum1.setId(TEST_ID_1);
        testAlbum1.setName(TEST_NAME_1);
        Album testAlbum2 = new Album();
        testAlbum2.setId(TEST_ID_2);
        testAlbum2.setName(TEST_NAME_2);
        List<Album> albumList = new ArrayList<>();
        albumList.add(testAlbum1);
        albumList.add(testAlbum2);
        List<JsonAlbumInfo> expected = new ArrayList<>();
        JsonAlbumInfo jsonAlbumInfo1 = new JsonAlbumInfo(TEST_ID_1,TEST_NAME_1);
        JsonAlbumInfo jsonAlbumInfo2 = new JsonAlbumInfo(TEST_ID_2,TEST_NAME_2);
        expected.add(jsonAlbumInfo1);
        expected.add(jsonAlbumInfo2);
        List<JsonAlbumInfo> actual = albumService.getJsonAlbumInfo(albumList);
        assertEquals(expected.size(), actual.size());
        assertThat(expected).containsExactlyElementsOf(actual);
    }

    @Test
    void shouldTriggerNullPointerExceptionWhenTryToGetJsonAlbumInfoWithEmptyValues() {
        AlbumService albumService = new AlbumJpaService();
        Album testAlbum = new Album();
        List<Album> albumList = new ArrayList<>();
        albumList.add(testAlbum);
        assertThrows(NullPointerException.class, () -> albumService.getJsonAlbumInfo(albumList));
    }

    @Test
    void shouldReturnNullWhenTryToFindAllAlbums() {
        Mockito.when(albumRepository.findAll()).thenReturn(null);
        assertNull(albumJpaService.findAll());
        Mockito.verify(albumRepository).findAll();
    }

    @Test
    void shouldDoNothingWhenTryToDeleteAlbumById() {
        Mockito.doNothing().when(albumRepository).deleteById(TEST_ID_1);
        albumJpaService.deleteById(TEST_ID_1);
        Mockito.verify(albumRepository, Mockito.only()).deleteById(TEST_ID_1);
    }

    @Test
    void shouldDoNothingWhenTryToSaveAlbumInDatabase() {
        Album testAlbum = new Album();
        Mockito.when(albumRepository.save(testAlbum)).thenReturn(testAlbum);
        albumJpaService.save(testAlbum);
        Mockito.verify(albumRepository, Mockito.only()).save(testAlbum);
    }

    @Test
    void shouldReturnTestAlbumWhenTryToFindAlbumById() {
        Optional<Album> testAlbumOptinal = Optional.empty();
        Mockito.when(albumRepository.findById(TEST_ID_1)).thenReturn(testAlbumOptinal);
        assertEquals(testAlbumOptinal, albumJpaService.findById(TEST_ID_1));
        Mockito.verify(albumRepository, Mockito.only()).findById(TEST_ID_1);
    }

    @Test
    void shouldReturnTestAlbumListWhenTryToFindAlbumBySinger() {
        Singer testSinger = new Singer();
        List<Album> testAlbumList = new ArrayList<>();
        testAlbumList.add(new Album());
        Mockito.when(albumRepository.findBySinger(testSinger)).thenReturn(testAlbumList);
        assertEquals(testAlbumList.size(), albumJpaService.findBySinger(testSinger).size());
        assertThat(testAlbumList).containsExactlyElementsOf(albumJpaService.findBySinger(testSinger));
        Mockito.verify(albumRepository, Mockito.times(2)).findBySinger(testSinger);
    }

    @Test
    void shouldReturnTestAlbumWhenTryTofindAlbumByContainingPartOfName() {
        List<Album> testAlbumList = new ArrayList<>();
        testAlbumList.add(new Album());
        Mockito.when(albumRepository.findByNameContains(TEST_NAME_1)).thenReturn(testAlbumList);
        assertEquals(testAlbumList.size(), albumJpaService.findByNameContains(TEST_NAME_1).size());
        assertThat(testAlbumList).containsExactlyElementsOf(albumJpaService.findByNameContains(TEST_NAME_1));
        Mockito.verify(albumRepository, Mockito.times(2)).findByNameContains(TEST_NAME_1);
    }
}