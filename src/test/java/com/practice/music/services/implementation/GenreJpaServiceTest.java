package com.practice.music.services.implementation;

import com.practice.music.entities.Genre;
import com.practice.music.repositories.GenreRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class GenreJpaServiceTest {

    private static final Integer TEST_ID_1 = 1;

    @InjectMocks
    private GenreJpaService genreJpaService;

    @Mock
    private GenreRepository genreRepository;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void shouldReturnNullWhenTryToFindAllGenres() {
        Mockito.when(genreRepository.findAll()).thenReturn(null);
        assertNull(genreJpaService.findAll());
        Mockito.verify(genreRepository).findAll();
    }

    @Test
    void shouldDoNothingWhenTryToDeleteGenreById() {
        Mockito.doNothing().when(genreRepository).deleteById(TEST_ID_1);
        genreJpaService.deleteById(TEST_ID_1);
        Mockito.verify(genreRepository, Mockito.only()).deleteById(TEST_ID_1);
    }

    @Test
    void shouldDoNothingWhenTryToSaveGenreInDatabase() {
        Genre testGenre = new Genre();
        Mockito.when(genreRepository.save(testGenre)).thenReturn(testGenre);
        genreJpaService.save(testGenre);
        Mockito.verify(genreRepository, Mockito.only()).save(testGenre);
    }

    @Test
    void shouldSucceedWhenTryToFindGenreById() {
        Mockito.when(genreRepository.findById(TEST_ID_1)).thenReturn(Optional.empty());
        genreJpaService.findById(TEST_ID_1);
        Mockito.verify(genreRepository, Mockito.only()).findById(TEST_ID_1);
    }

    @Test
    void shouldSucceedWhenTryfindAllByIds() {
        List<Integer> testIds = new ArrayList<>();
        testIds.add(TEST_ID_1);
        List<Genre> testGenres = new ArrayList<>();
        Mockito.when(genreRepository.findAllById(testIds)).thenReturn(testGenres);
        assertEquals(genreJpaService.findAllByIds(testIds), testGenres);
        Mockito.verify(genreRepository, Mockito.only()).findAllById(testIds);
    }
}