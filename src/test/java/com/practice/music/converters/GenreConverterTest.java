package com.practice.music.converters;

import com.practice.music.entities.Genre;
import com.practice.music.services.GenreService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@ExtendWith(MockitoExtension.class)
public class GenreConverterTest {

    private static final Integer TEST_ID = 1;
    private static final String TEST_NAME = "test";

    private Optional<Genre> testOptionalGenre;

    @InjectMocks
    private GenreConverter genreConverter;

    @Mock
    private GenreService genreService;

    @BeforeEach
    public void setUp(){
        Genre testGenre = new Genre();
        testGenre.setName(TEST_NAME);
        testGenre.setId(TEST_ID);
        testGenre.setSingerList(new ArrayList<>());
        testOptionalGenre = Optional.ofNullable(testGenre);
    }

    @Test
    public void shouldReturnSpecificGenreWhenTryToConvertIdIntoGenreEntity() {
        Mockito.when(genreService.findById(TEST_ID)).thenReturn(testOptionalGenre);
        assertEquals(genreConverter.convert(String.valueOf(TEST_ID)), testOptionalGenre.get());
        Mockito.verify(genreService).findById(TEST_ID);
    }

    @Test
    public void shouldReturnNullwhenTryToConvertIdIntoGenreEntity() {
        Mockito.when(genreService.findById(TEST_ID)).thenReturn(Optional.empty());
        assertNull(genreConverter.convert(String.valueOf(TEST_ID)));
        Mockito.verify(genreService).findById(TEST_ID);
    }
}