drop database if exists music;
create database music;
use music;
CREATE TABLE IF NOT EXISTS user (
    id INT AUTO_INCREMENT PRIMARY KEY,
    login VARCHAR(255) NOT NULL,
    password VARCHAR(255) NOT NULL,
	enabled boolean default true,
    role VARCHAR(255) default "USER"
)  ENGINE=INNODB;
INSERT INTO user(login,password,role)
VALUE('admin', '$2a$10$NH4El/Q.cgMOkcKeYBDVcur9RTCk2HZx5/TuPb/1Nf02EIB8swxY.','admin');
CREATE TABLE IF NOT EXISTS playlist (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    user_id INT,
		FOREIGN KEY (user_id)
        REFERENCES user (id)
        ON UPDATE CASCADE ON DELETE CASCADE
)  ENGINE=INNODB;
CREATE TABLE IF NOT EXISTS singer (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    logo VARCHAR(255) NOT NULL
)  ENGINE=INNODB;
INSERT INTO singer(name, logo)
VALUES('eminem', 'https://i.pinimg.com/originals/a6/17/fd/a617fd5d08b520e2fe333f84663994da.jpg'),
('bob marley','https://i.pinimg.com/originals/57/23/55/572355a40026747f4ecbc0751226096d.png'),
('system of a down','https://1000logos.net/wp-content/uploads/2020/02/System-of-a-Down-Logo.png'),
('beyonce', 'https://upload.wikimedia.org/wikipedia/commons/a/a1/Beyonce_Album_Logo.svg');
CREATE TABLE IF NOT EXISTS album (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    poster VARCHAR(255) NOT NULL,
    singer_id INT,
        FOREIGN KEY (singer_id)
        REFERENCES singer (id)
        ON UPDATE CASCADE ON DELETE CASCADE
)  ENGINE=INNODB;
INSERT INTO album(name, poster, singer_id)
VALUES('encore','https://static.stereogum.com/uploads/2019/02/10encore-1551108997.png', 1),
('the eminem show', 'https://upload.wikimedia.org/wikipedia/en/3/35/The_Eminem_Show.jpg', 1),
('legend', 'https://upload.wikimedia.org/wikipedia/ru/thumb/6/6d/Bob_Marley_-_Legend.jpeg/274px-Bob_Marley_-_Legend.jpeg',2),
('toxicity', 'https://upload.wikimedia.org/wikipedia/ru/thumb/e/e7/SOAD_Album_02.jpg/274px-SOAD_Album_02.jpg',3),
('lemonade', 'https://upload.wikimedia.org/wikipedia/ru/c/c0/Beyonce_-_Lemonade_%28album_cover%29.png',4);
CREATE TABLE IF NOT EXISTS song (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    link VARCHAR(255) NOT NULL,
    album_id INT,
        FOREIGN KEY (album_id)
        REFERENCES album (id)
        ON UPDATE CASCADE ON DELETE CASCADE
)  ENGINE=INNODB;
INSERT INTO song(name, link, album_id)
VALUES('just lose it','https://www.youtube.com/watch?v=9dcVOmEQzKA', 1),
('mockinbird','https://www.youtube.com/watch?v=S9bCLPwzSC0', 1),
('sing for the moment', 'https://www.youtube.com/watch?v=D4hAVemuQXY',2),
('catch a fire','https://www.youtube.com/watch?v=MLc7VY1jZwk',3),
('toxicity','https://www.youtube.com/watch?v=iywaBOMvYLI',4),
('sorry','https://www.youtube.com/watch?v=QxsmWxxouIM',5);
CREATE TABLE IF NOT EXISTS genre (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255) NOT NULL
)  ENGINE=INNODB;
INSERT INTO genre(name)
VALUES('rock'),('rap'),('classic music'),('pop');
CREATE TABLE IF NOT EXISTS playlist_song (
    playlist_id INT,
    song_id INT,
		FOREIGN KEY (playlist_id)
        REFERENCES playlist (id)
        ON UPDATE CASCADE ON DELETE CASCADE,
        FOREIGN KEY (song_id)
        REFERENCES song (id)
        ON UPDATE CASCADE ON DELETE CASCADE
)  ENGINE=INNODB;
CREATE TABLE IF NOT EXISTS singer_genre (
    id INT AUTO_INCREMENT PRIMARY KEY,
    singer_id INT,
    genre_id INT,
        FOREIGN KEY (singer_id)
        REFERENCES singer (id)
        ON UPDATE CASCADE ON DELETE CASCADE,
        FOREIGN KEY (genre_id)
        REFERENCES genre (id)
        ON UPDATE CASCADE ON DELETE CASCADE
)  ENGINE=INNODB;
INSERT INTO singer_genre(singer_id, genre_id)
VALUES(1,2),(2,3),(3,1),(4,4);
drop user 'springadmin';
flush privileges;
create user 'springadmin' identified by 'password';
grant all on music.* to 'springadmin';