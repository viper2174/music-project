$(document).ready(function() {
    $('.ajax-form').submit(function(event) {
        var currentForm, playlistId, songId;
        currentForm = $(this);
        var data = $(this).serializeArray();
        $.each(data,function(){
            if (this.name == 'playlistId') {
                playlistId = this.value;
            }
            if (this.name == 'songId') {
                songId = this.value;
            }
        });
        $.ajax({
            type        : 'GET',
            url         : '/addInPlaylist',
            dataType    : 'json',

            data        : {
                'playlistId': playlistId,
                'songId' : songId
            },
            success: function (data) {
                var result = data.result;
                var message = data.message;
                if (result == 'success') {
                    $('#' + songId).html(message);
                    $('#' + songId).removeClass('error');
                    $('#' + songId).addClass('success');
                //page refresh in 2.5 seconds with added playlist after success message is displayed
                    setTimeout(function(){
                        window.location.reload();
                    }, 2500);

                }
                if (result == 'failure') {
                    $('#' + songId).html(message);
                    $('#' + songId).removeClass('success');
                    $('#' + songId).class('error');
                //page refresh in 2.5 seconds with added playlist after success message is displayed
                    setTimeout(function(){
                        window.location.reload();
                    }, 2500);
                }
            }});
        event.preventDefault();
    });

});

