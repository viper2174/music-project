$(document).on("change", ".musicSelect", function () {
    var id = $(this).find(':selected')[0].value;
    $.ajax({
        type: 'GET',
        url: '/admin/getAlbumsByMusician',
        dataType: 'json',
        data: {
            'id': id
        },
        success: function (data) {
            var $album = $('.albm');
            $album.empty();
            if (jQuery.isEmptyObject(data)) {
                $album.append("<span class=\"error\">You can't add song because you need to create album first</span>");
                $album.closest('input[type="submit"]').attr('disabled','disabled');
            } else {
                for (var i = 0; i < data.length; i++) {
                    $album.append('<option value=' + data[i].id + '>' + data[i].name + '</option>');
                }
                $album.change();
            }

        }
    });
});
