$(document).ready(function() {
    $('.edit-playlist').click(function(event) {
        $(this).find('.form-popup').toggle();
    });

    $('.remove').click(function(event) {
    var r = confirm("Are you sure you wanna remove this playlist?");
    if (r == true) {
    var playlistId = this.getAttribute('value');
          $.post("removePlaylist",
          {
              id: playlistId
          },
          function(data, xhr){
              if (xhr == 'success') {
                 window.location.reload();
              }
          });
    }
    });

});

