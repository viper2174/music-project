package com.practice.music.services;

import com.practice.music.entities.Album;
import com.practice.music.entities.Singer;
import com.practice.music.models.JsonAlbumInfo;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public interface AlbumService {

    List<JsonAlbumInfo> getJsonAlbumInfo(List<Album> albums);
    List<Album> findAll();
    void deleteById(Integer valueOf);
    Album save(Album album);
    Optional<Album> findById(int albumId);
    List<Album> findBySinger(Singer singer);
    List<Album> findByNameContains(String query);

}
