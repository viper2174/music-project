package com.practice.music.services;

import com.practice.music.entities.Genre;

import java.util.List;
import java.util.Optional;

public interface GenreService {

    List<Genre> findAll();
    void deleteById(Integer valueOf);
    Genre save(Genre genre);
    Optional<Genre> findById(Integer valueOf);
    List<Genre> findAllByIds(List<Integer> ids);
}
