package com.practice.music.services;

import com.practice.music.entities.Singer;

import java.util.List;
import java.util.Optional;

public interface SingerService {

    List<Singer> findAll();
    void deleteById(Integer valueOf);
    Singer save(Singer singer);
    Optional<Singer> findById(int musicianId);
    List<Singer> findByNameContains(String query);
}
