package com.practice.music.services;

import com.practice.music.entities.Album;
import com.practice.music.entities.Song;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface SongService {

    List<Song> findAll();
    void deleteById(Integer valueOf);
    Song save(Song song);
    List<Song> findByNameContains(String query);
    List<Song> findByAlbum(Album album);
    Optional<Song> findById(Integer integer);
    Page<Song> findPaginated (Pageable pageable);
    Page<Song> findByNameContains(String query, Pageable pageable);

}
