package com.practice.music.services;

import com.practice.music.entities.User;
import com.practice.music.models.UserForm;
import org.springframework.security.core.Authentication;

import java.util.Optional;

public interface UserService {

    Optional<User> findByLogin(String login);
    User getUserFromUserForm(UserForm userForm);
    User save(User user);
    Optional<User> getUserData(Authentication authentication);

}
