package com.practice.music.services.implementation;

import com.practice.music.entities.Singer;
import com.practice.music.repositories.SingerRepository;
import com.practice.music.services.SingerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class SingerJpaService implements SingerService {

    @Autowired
    private SingerRepository singerRepository;

    @Override
    public List<Singer> findAll() {
        return singerRepository.findAll();
    }

    @Transactional
    @Override
    public void deleteById(Integer id) {
        singerRepository.deleteById(id);
    }

    @Transactional
    @Override
    public Singer save(Singer singer) {
        return singerRepository.save(singer);
    }

    @Override
    public Optional<Singer> findById(int musicianId) {
        return singerRepository.findById(musicianId);
    }

    @Override
    public List<Singer> findByNameContains(String query) {
        return singerRepository.findByNameContains(query);
    }
}
