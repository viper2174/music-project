package com.practice.music.services.implementation;

import com.practice.music.entities.Album;
import com.practice.music.entities.Singer;
import com.practice.music.models.JsonAlbumInfo;
import com.practice.music.repositories.AlbumRepository;
import com.practice.music.services.AlbumService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class AlbumJpaService implements AlbumService {

    @Autowired
    private AlbumRepository albumRepository;

    @Override
    public List<JsonAlbumInfo> getJsonAlbumInfo(List<Album> albums) {
        return albums.stream().map(a -> new JsonAlbumInfo(a.getId(), a.getName())).collect(Collectors.toList());
    }

    @Override
    public List<Album> findAll() {
        return albumRepository.findAll();
    }

    @Transactional
    @Override
    public void deleteById(Integer id) {
        albumRepository.deleteById(id);
    }

    @Transactional
    @Override
    public Album save(Album album) {
        return albumRepository.save(album);
    }

    @Override
    public Optional<Album> findById(int albumId) {
        return albumRepository.findById(albumId);
    }

    @Override
    public List<Album> findBySinger(Singer singer) {
        return albumRepository.findBySinger(singer);
    }

    @Override
    public List<Album> findByNameContains(String query) {
        return albumRepository.findByNameContains(query);
    }
}
