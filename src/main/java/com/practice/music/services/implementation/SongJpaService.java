package com.practice.music.services.implementation;

import com.practice.music.entities.Album;
import com.practice.music.entities.Song;
import com.practice.music.repositories.SongRepository;
import com.practice.music.services.SongService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class SongJpaService implements SongService {

    @Autowired
    private SongRepository songRepository;

    @Override
    public List<Song> findAll() {
        return songRepository.findAll();
    }

    @Transactional
    @Override
    public void deleteById(Integer id) {
        songRepository.deleteById(id);
    }

    @Transactional
    @Override
    public Song save(Song song) {
        return songRepository.save(song);
    }

    @Override
    public List<Song> findByNameContains(String query) {
        return songRepository.findByNameContains(query);
    }

    @Override
    public List<Song> findByAlbum(Album album) {
        return songRepository.findByAlbum(album);
    }

    @Override
    public Optional<Song> findById(Integer id) {
        return songRepository.findById(id);
    }

    @Override
    public Page<Song> findPaginated(Pageable pageable) {
        return songRepository.findAll(pageable);
    }

    @Override
    public Page<Song> findByNameContains(String query, Pageable pageable) {
        return songRepository.findByNameContains(query, pageable);
    }
}
