package com.practice.music.services.implementation;

import com.practice.music.entities.Playlist;
import com.practice.music.entities.Song;
import com.practice.music.entities.User;
import com.practice.music.models.JsonPlaylistResponse;
import com.practice.music.repositories.PlaylistRepository;
import com.practice.music.repositories.SongRepository;
import com.practice.music.repositories.UserRepository;
import com.practice.music.services.PlaylistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PlaylistJpaService implements PlaylistService {

    private static final Integer NEW_PLAYLIST_VALUE = 0;
    private static final String SUCCESS = "success";
    private static final String ERROR = "error";
    private static final String NEW_PLAYLIST_ADDED_MESSAGE = "new playlist has been added";
    private static final String SONG_ADDED_TO_PLAYLIST_MESSAGE = "song has been added to playlist";
    private static final String PLAYLIST_WITH_ID_NOT_EXIST = "playlist with such id doesn't exist";

    @Autowired
    private PlaylistRepository playlistRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private SongRepository songRepository;

    @Transactional
    @Override
    public Playlist save(Playlist playlist) {
        return playlistRepository.save(playlist);
    }

    @Override
    public List<Playlist> findAll() {
        return playlistRepository.findAll();
    }

    @Transactional
    @Override
    public JsonPlaylistResponse saveSongInPlaylistAndObtainJson(Song song, User user, int playlistId) {
        JsonPlaylistResponse response;
        if (playlistId == NEW_PLAYLIST_VALUE) {
            Playlist playlist = new Playlist();
            playlist.setName(generatePlaylistName(song));
            playlist.setUser(user);
            List<Song> songList = new ArrayList<>();
            songList.add(song);
            playlist.setSongList(songList);
            Playlist dbPlaylist = playlistRepository.save(playlist);
            response = new JsonPlaylistResponse(SUCCESS, NEW_PLAYLIST_ADDED_MESSAGE, dbPlaylist.getId());
        } else {
            if (addSongIntoPlaylist(song, playlistId)) {
                response = new JsonPlaylistResponse(SUCCESS, SONG_ADDED_TO_PLAYLIST_MESSAGE, playlistId);
            } else {
                response = new JsonPlaylistResponse(ERROR, PLAYLIST_WITH_ID_NOT_EXIST, playlistId);
            }
        }
        return response;
    }

    private String generatePlaylistName(Song song) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(song.getName()).append(" playlist");
        return stringBuilder.toString();
    }

    public boolean addSongIntoPlaylist(Song song, Integer playlistId) {
        Optional<Playlist> playlistOptional = playlistRepository.findById(playlistId);
        if (playlistOptional.isPresent()) {
            Playlist playlist = playlistOptional.get();
            List<Song> songList = playlist.getSongList();
            if(!songList.contains(song)) {
                songList.add(song);
            }
            playlist.setSongList(songList);
            playlistRepository.save(playlist);
            return true;
        }
        return false;
    }

    @Override
    public Optional<Playlist> findById(int playlistId) {
        return playlistRepository.findById(playlistId);
    }

    @Transactional
    @Override
    public void deleteById(int playlistId, User user) {
        List<Playlist> playlistList = user.getPlaylistList();
        playlistList.removeIf(p -> p.getId() == playlistId);
        user.setPlaylistList(playlistList);
        userRepository.save(user);
        playlistRepository.deleteById(playlistId);
    }

    @Transactional
    @Override
    public boolean editPlaylist(int playlistId, String newName) {
        Optional<Playlist> playlistOptional = playlistRepository.findById(playlistId);
        if (playlistOptional.isPresent()) {
            Playlist playlist = playlistOptional.get();
            playlist.setName(newName);
            playlistRepository.save(playlist);
            return true;
        } else {
            return false;
        }
    }

    @Transactional
    @Override
    public void removeSongFromPlaylist(int playlistId, int songId) {
        Optional<Playlist> playlistOptional = playlistRepository.findById(playlistId);
        Optional<Song> songOptional = songRepository.findById(songId);
        if (playlistOptional.isPresent()) {
            Playlist playlist = playlistOptional.get();
            List<Song> songs = playlist.getSongList();
            songs.removeIf(s -> s.getId() == songId);
            playlist.setSongList(songs);
            playlistRepository.save(playlist);
            if (songOptional.isPresent()) {
                Song song = songOptional.get();
                List<Playlist> playlistList = song.getPlaylistList();
                playlistList.removeIf(p -> p.getId() == playlistId);
                song.setPlaylistList(playlistList);
                songRepository.save(song);
            }
        }
    }
}

