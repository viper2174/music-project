package com.practice.music.services.implementation;

import com.practice.music.entities.Genre;
import com.practice.music.repositories.GenreRepository;
import com.practice.music.services.GenreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class GenreJpaService implements GenreService {

    @Autowired
    private GenreRepository genreRepository;

    @Override
    public List<Genre> findAll() {
        return genreRepository.findAll();
    }

    @Transactional
    @Override
    public void deleteById(Integer id) {
        genreRepository.deleteById(id);
    }

    @Transactional
    @Override
    public Genre save(Genre genre) {
        return genreRepository.save(genre);
    }

    @Override
    public Optional<Genre> findById(Integer id) {
        return genreRepository.findById(id);
    }

    @Override
    public List<Genre> findAllByIds(List<Integer> ids) {
        return genreRepository.findAllById(ids);
    }
}
