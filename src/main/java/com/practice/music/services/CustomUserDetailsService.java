package com.practice.music.services;

import com.practice.music.entities.User;
import com.practice.music.models.CustomUserDetails;
import com.practice.music.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
public class CustomUserDetailsService implements UserDetailsService {

    private static final String USERNAME = "Username";
    private static final String NOT_FOUND = "has been not found";

    @Autowired
    private UserRepository userRepository;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        Optional<User> optionalUser = userRepository.findByLogin(login);
        if (optionalUser.isPresent()) {
            return optionalUser.map(CustomUserDetails::new).get();
        } else {
            String errorMessage = new StringBuilder().append(USERNAME).append(login)
                    .append(NOT_FOUND).toString();
            throw new UsernameNotFoundException(errorMessage);
        }
    }
}
