package com.practice.music.services;

import com.practice.music.entities.Playlist;
import com.practice.music.entities.Song;
import com.practice.music.entities.User;
import com.practice.music.models.JsonPlaylistResponse;

import java.util.List;
import java.util.Optional;

public interface PlaylistService {

    Playlist save(Playlist playlist);
    boolean addSongIntoPlaylist(Song song, Integer playlistId);
    List<Playlist> findAll();
    JsonPlaylistResponse saveSongInPlaylistAndObtainJson(Song song, User user, int playlistId);
    Optional<Playlist> findById(int playlistId);
    void deleteById(int playlistId, User user);
    boolean editPlaylist(int playlistId, String newName);
    void removeSongFromPlaylist(int playlistId, int songId);
}
