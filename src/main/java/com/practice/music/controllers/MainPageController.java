package com.practice.music.controllers;

import com.practice.music.entities.Album;
import com.practice.music.entities.Singer;
import com.practice.music.entities.Song;
import com.practice.music.models.PlaylistUpdateForm;
import com.practice.music.services.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.thymeleaf.util.StringUtils;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Controller
public class MainPageController {

    private static final Integer DEFAULT_PAGGINATION_PAGE = 1;
    private static final Integer DEFAULT_PAGGINATION_SIZE = 3;
    private static final String EMPTY_QUERY = "";

    Logger logger = LoggerFactory.getLogger(MainPageController.class);

    @Autowired
    private GenreService genreService;

    @Autowired
    private AlbumService albumService;

    @Autowired
    private SingerService singerService;

    @Autowired
    private SongService songService;

    @Autowired
    private PlaylistService playlistService;

    @Autowired
    private UserService userService;

    @GetMapping(path = "/")
    public String getMainPage(Model model) {
        model.addAttribute("playlistUpdateForm", new PlaylistUpdateForm());
        getSearchResultWithAllSongsPaginated(DEFAULT_PAGGINATION_PAGE, DEFAULT_PAGGINATION_SIZE, model);
        return "mainPage";
    }

    private void getSearchResultWithAllSongsPaginated(int currentPage, int pageSize, Model model) {
        Page<Song> songPage = songService.findPaginated(PageRequest.of(currentPage - 1, pageSize));
        model.addAttribute("songPage", songPage);
        List<Integer> pageNumbers = obtainPageNumbersAndAddThemToResponse(songPage);
        model.addAttribute("pageNumbers", pageNumbers);
        model.addAttribute("foundSongs", songPage);
    }

    private List<Integer> obtainPageNumbersAndAddThemToResponse(Page<Song> songPage) {
        int totalPages = songPage.getTotalPages();
        return IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());
    }

    @GetMapping(path = "/search")
    public String getSearchResultOnMainPage(@RequestParam (name = "q", required = false)Optional<String> query,
                                            @RequestParam("page") Optional<Integer> page,
                                            @RequestParam("size") Optional<Integer> size, Model model) {
        int currentPage = page.orElse(DEFAULT_PAGGINATION_PAGE);
        int pageSize = size.orElse(DEFAULT_PAGGINATION_SIZE);
        String q = query.orElse(EMPTY_QUERY);
        model.addAttribute("playlistUpdateForm", new PlaylistUpdateForm());
        model.addAttribute("q",q);
        if (StringUtils.isEmpty(q)){
            getSearchResultWithAllSongsPaginated(currentPage, pageSize, model);
        } else {
            getSearchResultWithFilteredSongsPaginated(currentPage, pageSize, q, model);
        }
        return "mainPage";
    }

    private void getSearchResultWithFilteredSongsPaginated(int currentPage, int pageSize, String q, Model model) {
        List<Song> songList = songService.findByNameContains(q);
        Page<Song> songPage = songService.findByNameContains(q, PageRequest.of(currentPage - 1, pageSize));
        logger.info("songListByQuery" + songList);
        logger.info("songListByQueryPaggination" + songPage);
        List<Integer> pageNumbers = obtainPageNumbersAndAddThemToResponse(songPage);
        model.addAttribute("pageNumbers", pageNumbers);
        model.addAttribute("foundSingers", singerService.findByNameContains(q));
        model.addAttribute("foundSongs", songPage);
        model.addAttribute("foundAlbums", albumService.findByNameContains(q));
    }

    @GetMapping(path = "/singer")
    public String getSingerDescription(@RequestParam (name = "id") Optional<Integer> optionalId, Model model){
        if (optionalId.isPresent()){
            Optional<Singer> optionalSinger = singerService.findById(optionalId.get());
            addFoundSinger (optionalSinger, model);
        }
            return "singerInfo";
    }

    private void addFoundSinger(Optional<Singer> optionalSinger, Model model) {
        if (optionalSinger.isPresent()) {
            model.addAttribute("singer", optionalSinger.get());
            model.addAttribute("albumList", albumService.findBySinger(optionalSinger.get()));
        }
    }

    @GetMapping(path = "/album")
    public String getAlbumDescription(@RequestParam (name = "id") Optional<Integer> optionalId, Model model){
        if (optionalId.isPresent()){
            Optional<Album> optionalAlbum = albumService.findById(optionalId.get());
            addFoundAlbum (optionalAlbum, model);
        }
        return "albumInfo";
    }

    private void addFoundAlbum(Optional<Album> optionalAlbum, Model model) {
        if (optionalAlbum.isPresent()) {
            model.addAttribute("album", optionalAlbum.get());
            model.addAttribute("songList", songService.findByAlbum(optionalAlbum.get()));
        }
    }

    @GetMapping(path = "/song")
    public String getSongDescription(@RequestParam (name = "id") Optional<Integer> optionalId, Model model){
        if (optionalId.isPresent()){
            Optional<Song> optionalSong = songService.findById(optionalId.get());
            addFoundSong (optionalSong, model);
        }
        return "songInfo";
    }

    private void addFoundSong(Optional<Song> optionalSong, Model model) {
        optionalSong.ifPresent(song -> model.addAttribute("song", song));
    }

}