package com.practice.music.controllers;

import com.practice.music.entities.User;
import com.practice.music.models.UserForm;
import com.practice.music.services.UserService;
import com.practice.music.validators.UserFormValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;

@Controller
public class RegistrationController {

    public static final String REGISTRATION_SUCCESSFUL = "Registration has been successful";

    @Autowired
    private UserFormValidator userFormValidator;
    @Autowired
    private UserService userService;

    //set userFormValidator as a validator which will be invoked in case of UserForm usage
    @InitBinder
    protected void initBinder(WebDataBinder dataBinder) {
        Object target = dataBinder.getTarget();
        if (target == null) {
            return;
        }
        if (target.getClass() == UserForm.class) {
            dataBinder.setValidator(userFormValidator);
        }
    }

    @GetMapping("/registration")
    public ModelAndView getRegisterPage(){
        ModelAndView modelAndView = new ModelAndView("registration");
        modelAndView.addObject("userForm",new UserForm());
        return modelAndView;
    }

    @PostMapping("/registration/register")
    public ModelAndView registerNewUser (@ModelAttribute("userForm") @Validated UserForm userForm,
                                   BindingResult bindingResult, HttpSession httpSession) {
        if (bindingResult.hasErrors()){
            return new ModelAndView("registration");
        }
        User user = userService.getUserFromUserForm(userForm);
        userService.save(user);
        httpSession.setAttribute("registerSuccessful", REGISTRATION_SUCCESSFUL);
        return new ModelAndView("redirect:/login");
    }

}
