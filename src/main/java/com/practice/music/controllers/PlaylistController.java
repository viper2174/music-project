package com.practice.music.controllers;

import com.google.gson.Gson;
import com.practice.music.entities.Playlist;
import com.practice.music.entities.Song;
import com.practice.music.entities.User;
import com.practice.music.models.JsonPlaylistResponse;
import com.practice.music.services.PlaylistService;
import com.practice.music.services.SongService;
import com.practice.music.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.net.URL;
import java.util.Optional;

@Controller
public class PlaylistController {

    private static final String USER_NOT_AUTHORIZED = "user is not authorized. Authorize pls to use playlist feature";
    private static final String SUCCESS = "success";
    private static final String FAILURE = "failure";
    private static final String SONG_WITH_ID_NOT_EXIST = "song with such id does not exist";
    private static final int EMPTY_ID = 0;
    private static final String PLAYLIST_WITH_SUCH_ID_DOES_NOT_EXIST = "playlist with such id does not exist";
    private static final String PLAYLIST_IS_UPDATED = "playlist is updated";
    private static final String PLAYLIST_IS_NOT_UPDATED = "playlist is not updated";
    private static final String NAME_IS_EMPTY = "name is empty";
    private static final String ERROR_PLAYLIST_INVALID_ID = "playlist with specified id does not exist";
    private static final String MAIN_PAGE_REFERER = "https://localhost:8080/";

    private Logger logger = LoggerFactory.getLogger(PlaylistController.class);

    @Autowired
    private PlaylistService playlistService;

    @Autowired
    private SongService songService;

    @Autowired
    private UserService userService;

    @GetMapping("/playlist")
    private ModelAndView getPlaylistPage (@RequestParam ("id") int playlistId) {
        Optional<Playlist> playlistOptional = playlistService.findById(playlistId);
        if (playlistOptional.isPresent()) {
            ModelAndView modelAndView = new ModelAndView("user/playlist");
            modelAndView.addObject("playlist", playlistOptional.get());
            return modelAndView;
        } else {
            ModelAndView modelAndView = new ModelAndView("error");
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(PLAYLIST_WITH_SUCH_ID_DOES_NOT_EXIST).append(" ").append(playlistId);
            modelAndView.addObject("error", stringBuilder.toString());
            return modelAndView;
        }
    }

    @GetMapping("/addInPlaylist")
    @ResponseBody
    private String addSongIntoPlaylistOrCreateNew(@RequestParam ("playlistId") int playlistId,
                                                  @RequestParam ("songId") int songId, HttpSession httpSession){
        Gson gson = new Gson();
        JsonPlaylistResponse response;
        Optional<Song> optionalSong = songService.findById(songId);
        Optional<User> optionalUser = Optional.ofNullable((User) httpSession.getAttribute("user"));
        if (optionalSong.isPresent() && optionalUser.isPresent()) {
            response = playlistService.saveSongInPlaylistAndObtainJson(optionalSong.get(), optionalUser.get(), playlistId);
            User oldUserData = (User) httpSession.getAttribute("user");
            Optional<User> updatedUserOptional = userService.findByLogin(oldUserData.getLogin());
            updatedUserOptional.ifPresent(user -> httpSession.setAttribute("user", user));
        } else if (!optionalUser.isPresent()){
            response = new JsonPlaylistResponse(FAILURE, USER_NOT_AUTHORIZED, EMPTY_ID);
        } else {
            response = new JsonPlaylistResponse(FAILURE, SONG_WITH_ID_NOT_EXIST, EMPTY_ID);
        }
        return gson.toJson(response);
    }

    @PostMapping("/removePlaylist")
    @ResponseBody
    private ResponseEntity<String> removePlaylistById(@RequestParam ("id") Optional<Integer> playlistId,
                                                      HttpSession httpSession) {
        if (playlistId.isPresent()) {
            User oldUserData = (User) httpSession.getAttribute("user");
            playlistService.deleteById(playlistId.get(), oldUserData);
            Optional<User> updatedUserOptional = userService.findByLogin(oldUserData.getLogin());
            updatedUserOptional.ifPresent(user -> httpSession.setAttribute("user", user));
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    private String getURIByReferer(String referer) throws Exception{
        URL url = new URL(referer);
        return url.getPath();
    }

    @PostMapping("/editPlaylist")
    private String editPlaylist(@RequestHeader(value = "referer", required = false) final Optional<String> refererHeader,
                                @RequestParam ("id") Optional<Integer> playlistId,
                                @RequestParam ("name") Optional<String> newName, HttpSession httpSession) throws Exception{
        String referer = refererHeader.orElse(MAIN_PAGE_REFERER);
        if (playlistId.isPresent() && newName.isPresent()) {
            playlistService.editPlaylist(playlistId.get(), newName.get());
            User oldUserData = (User) httpSession.getAttribute("user");
            Optional<User> updatedUserOptional = userService.findByLogin(oldUserData.getLogin());
            updatedUserOptional.ifPresent(user -> httpSession.setAttribute("user", user));
        }
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("redirect:").append(getURIByReferer(referer));
        return stringBuilder.toString();
    }

    @PostMapping("/removeSongFromPlaylist")
    private String removeSongFromPlaylist(@RequestParam("songId") Optional<Integer> songId,
                                          @RequestParam("playlistId") Optional<Integer> playlistId,
                                          Model model, HttpSession httpSession) {
        if (playlistId.isPresent() && songId.isPresent()){
            logger.info(String.valueOf(playlistId.get())+songId.get());
            playlistService.removeSongFromPlaylist(playlistId.get(), songId.get());
            User oldUserData = (User) httpSession.getAttribute("user");
            Optional<User> updatedUserOptional = userService.findByLogin(oldUserData.getLogin());
            updatedUserOptional.ifPresent(user -> httpSession.setAttribute("user", user));
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("redirect:/playlist?id=").append(playlistId.get());
            String query = stringBuilder.toString();
            return query;
        } else {
            model.addAttribute("error", ERROR_PLAYLIST_INVALID_ID);
            return "error";
        }
    }
}
