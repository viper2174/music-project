package com.practice.music.controllers;

import com.practice.music.entities.User;
import com.practice.music.models.LoginForm;
import com.practice.music.services.UserService;
import com.practice.music.validators.LoginFormValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpSession;
import java.util.Optional;

@Controller
public class LoginController {

    @Autowired
    private UserService userService;

    @Autowired
    private LoginFormValidator validator;

    //set userFormValidator as a validator which will be invoked in case of UserForm usage
    @InitBinder
    protected void initBinder(WebDataBinder dataBinder) {
        Object target = dataBinder.getTarget();
        if (target == null) {
            return;
        }
        if (target.getClass() == LoginForm.class) {
            dataBinder.setValidator(validator);
        }
    }

    @GetMapping(path = "/login")
    public String getLoginPage (Model model){
        model.addAttribute("loginForm", new LoginForm());
        return "login";
    }
    @PostMapping(path = "/login/check")
    private String checkLogin (@ModelAttribute ("loginForm") @Validated LoginForm loginForm,
                               BindingResult bindingResult, HttpSession session){
        if (bindingResult.hasErrors()){
            return "/login";
        }
        Optional<User> userOptional = userService.findByLogin(loginForm.getLogin());
        if (userOptional.isPresent()) {
            User user = userOptional.get();
            session.setAttribute("user", user);
            if (user.getRole().equals("admin")) {
                return "redirect:/admin";
            } else {
                return "redirect:/profile";
            }
        } else {
            return "/login";
        }
    }

}
