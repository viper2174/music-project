package com.practice.music.controllers;

import com.google.gson.Gson;
import com.practice.music.entities.Album;
import com.practice.music.entities.Genre;
import com.practice.music.entities.Singer;
import com.practice.music.entities.Song;
import com.practice.music.models.*;
import com.practice.music.services.AlbumService;
import com.practice.music.services.GenreService;
import com.practice.music.services.SingerService;
import com.practice.music.services.SongService;
import com.practice.music.validators.SingerFormValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.Optional;

@Controller
public class AdminController {

    private static final String ADD_MUSICIAN_PAGE = "/admin/addMusicianPage";
    private static final String ADD_ALBUM_PAGE = "/admin/addAlbumPage";
    private static final String ADD_SONG_PAGE = "/admin/addSongPage";
    private static final String ADD_GENRE_PAGE = "/admin/addGenrePage";
    private static final String ID_SHOULD_BE_NUMBER = "id should be the numeric value";
    private static final String SINGER_WITH_ID_DOES_NOT_EXIST = "singer with specified id does not exist";
    private static final String ALBUM_WITH_ID_DOES_NOT_EXIST = "album with specified id does not exist";


    Logger logger = LoggerFactory.getLogger(AdminController.class);

    @Autowired
    private GenreService genreService;

    @Autowired
    private SingerService singerService;

    @Autowired
    private SongService songService;

    @Autowired
    private AlbumService albumService;

    @Autowired
    private SingerFormValidator validator;

    //set singerFormValidator as a validator which should be invoked in case of SingerForm usage
    @InitBinder
    protected void initBinder(WebDataBinder dataBinder) {
        Object target = dataBinder.getTarget();
        if (target == null) {
            return;
        }
        if (target.getClass() == SingerForm.class) {
            dataBinder.setValidator(validator);
        }
    }

    @GetMapping(path = "admin")
    private String getAdminPage(){
        return "/admin";
    }

    @GetMapping(path = "admin/addMusicianPage")
    private ModelAndView addMusicianPage(){
        ModelAndView modelAndView = new ModelAndView(ADD_MUSICIAN_PAGE);
        modelAndView.addObject("musicians", singerService.findAll());
        modelAndView.addObject("genreList", genreService.findAll());
        modelAndView.addObject("singerForm", new SingerForm());
        return modelAndView;
    }

    @GetMapping(path = "admin/addAlbumPage")
    private ModelAndView addAlbumPage(){
        ModelAndView modelAndView = new ModelAndView(ADD_ALBUM_PAGE);
        modelAndView.addObject("albums", albumService.findAll());
        modelAndView.addObject("musicians", singerService.findAll());
        modelAndView.addObject("albumForm", new AlbumForm());
        return modelAndView;
    }

    @GetMapping(path = "admin/addSongPage")
    private ModelAndView addSongPage(){
        ModelAndView modelAndView = new ModelAndView(ADD_SONG_PAGE);
        modelAndView.addObject("songs", songService.findAll());
        modelAndView.addObject("musicians", singerService.findAll());
        modelAndView.addObject("albums", albumService.findAll());
        modelAndView.addObject("songForm", new SongForm());
        return modelAndView;
    }

    @GetMapping(path = "admin/addGenrePage")
    private ModelAndView getAddGenrePage(){
        ModelAndView modelAndView = new ModelAndView(ADD_GENRE_PAGE);
        modelAndView.addObject("genres", genreService.findAll());
        modelAndView.addObject("genreForm", new GenreForm());
        return modelAndView;
    }

    @PostMapping(path = "admin/removeMusician/{someID}")
    private String removeMusician(@PathVariable(value="someID") String id, Model model){
        try {
            singerService.deleteById(Integer.valueOf(id));
            model.addAttribute("musicians", singerService.findAll());
            return "redirect:/admin/addMusicianPage";
        } catch (NumberFormatException ex) {
            model.addAttribute("error", ID_SHOULD_BE_NUMBER);
            model.addAttribute("songs", songService.findAll());
            model.addAttribute("musicians", singerService.findAll());
            model.addAttribute("albums", albumService.findAll());
            model.addAttribute("singerForm", new SingerForm());
            return "/admin/addMusicianPage";
        }
    }

    @PostMapping(path = "admin/removeAlbum/{someID}")
    private String removeAlbum(@PathVariable(value="someID") String id, Model model){
        try {
            albumService.deleteById(Integer.valueOf(id));
            model.addAttribute("musicians", singerService.findAll());
            model.addAttribute("albums", albumService.findAll());
            return "redirect:/admin/addAlbumPage";
        } catch (NumberFormatException ex) {
            model.addAttribute("error", ID_SHOULD_BE_NUMBER);
            model.addAttribute("albums", albumService.findAll());
            model.addAttribute("musicians", singerService.findAll());
            model.addAttribute("albumForm", new AlbumForm());
            return "/admin/addAlbumPage";
        }
    }

    @PostMapping(path = "admin/removeSong/{someID}")
    private String removeSong(@PathVariable(value="someID") String id, Model model){
        try {
            Integer exactId = Integer.valueOf(id);
            songService.deleteById(exactId);
            model.addAttribute("musicians", singerService.findAll());
            model.addAttribute("albums", albumService.findAll());
            model.addAttribute("songs", songService.findAll());
            return "redirect:/admin/addSongPage";
        } catch (NumberFormatException ex) {
            model.addAttribute("error", ID_SHOULD_BE_NUMBER);
            model.addAttribute("songs", songService.findAll());
            model.addAttribute("musicians", singerService.findAll());
            model.addAttribute("albums", albumService.findAll());
            model.addAttribute("songForm", new SongForm());
            return "/admin/addSongPage";
        }
    }

    @PostMapping(path = "admin/removeGenre/{someID}")
    private String removeGenre(@PathVariable(value="someID") String id, Model model){
        try {
            genreService.deleteById(Integer.valueOf(id));
            model.addAttribute("genres", genreService.findAll());
            return "redirect:/admin/addGenrePage";
        } catch (NumberFormatException ex) {
            model.addAttribute("error", ID_SHOULD_BE_NUMBER);
            model.addAttribute("genres", genreService.findAll());
            model.addAttribute("genreForm", new GenreForm());
            return "/admin/addGenrePage";
        }
    }

    @PostMapping(path = "admin/addNewGenre")
    private String addNewGenre(@ModelAttribute GenreForm genreForm, BindingResult bindingResult) {
        Genre genre = new Genre();
        genre.setName(genreForm.getName());
        genreService.save(genre);
        return "redirect:/admin/addGenrePage";
    }

    @PostMapping(path = "admin/addNewSinger")
    private String addNewSinger(@ModelAttribute @Validated SingerForm singerForm, BindingResult bindingResult) {
        if (bindingResult.hasErrors()){
            return "/admin/addMusicianPage";
        } else {
            Singer singer = new Singer();
            singer.setName(singerForm.getName());
            singer.setLogo(singerForm.getLogo());
            List<Genre> genres = genreService.findAllByIds(singerForm.getGenres());
            singer.setGenreList(genres);
            singerService.save(singer);
            return "redirect:/admin/addMusicianPage";
        }
    }

    @PostMapping(path = "admin/addNewAlbum")
    private String addNewAlbum(@ModelAttribute AlbumForm albumForm, BindingResult bindingResult) {
        Album album = new Album();
        album.setName(albumForm.getName());
        album.setPoster(albumForm.getPoster());
        Optional<Singer> optionalSinger = singerService.findById(albumForm.getMusicianId());
        if (optionalSinger.isPresent()){
            album.setSinger(optionalSinger.get());
            albumService.save(album);
            return "redirect:/admin/addAlbumPage";
        } else {
            bindingResult.addError(new ObjectError("singerNotFound",
                    SINGER_WITH_ID_DOES_NOT_EXIST));
            return "/admin/addAlbumPage";
        }
    }

    @PostMapping(path = "admin/addNewSong")
    private String addNewSong(@ModelAttribute SongForm songForm, BindingResult bindingResult) {
        Song song = new Song();
        song.setName(songForm.getName());
        song.setLink(songForm.getLink());
        Optional<Album> optionalAlbum = albumService.findById(songForm.getAlbumId());
        if (optionalAlbum.isPresent()){
            song.setAlbum(optionalAlbum.get());
            songService.save(song);
            return "redirect:/admin/addSongPage";
        } else {
            bindingResult.addError(new ObjectError("albumNotFound",
                    ALBUM_WITH_ID_DOES_NOT_EXIST));
            return "/admin/addSongPage";
        }
    }

    @GetMapping(path = "admin/getAlbumsByMusician")
    @ResponseBody
    private String getAlbumsByMusician(@RequestParam("id") int id){
        logger.info("ajaxMusicianId = " + id);
        Optional<Singer> optionalSinger = singerService.findById(id);
        if (optionalSinger.isPresent()){
            List<Album> albums = albumService.findBySinger(optionalSinger.get());
            List<JsonAlbumInfo> jsonAlbumInfoList = albumService.getJsonAlbumInfo(albums);
            logger.info(String.valueOf(jsonAlbumInfoList));
            Gson gson = new Gson();
            return gson.toJson(jsonAlbumInfoList);
        }
        return null;
    }
}
