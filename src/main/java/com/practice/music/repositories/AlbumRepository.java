package com.practice.music.repositories;

import com.practice.music.entities.Album;
import com.practice.music.entities.Singer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AlbumRepository extends JpaRepository<Album, Integer> {

    List<Album> findBySinger(Singer singer);
    List<Album> findByNameContains(String query);

}
