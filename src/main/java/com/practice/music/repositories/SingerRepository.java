package com.practice.music.repositories;

import com.practice.music.entities.Singer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface SingerRepository extends JpaRepository<Singer, Integer> {
    List<Singer> findByNameContains(String query);
}
