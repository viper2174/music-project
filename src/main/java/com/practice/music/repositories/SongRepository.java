package com.practice.music.repositories;

import com.practice.music.entities.Album;
import com.practice.music.entities.Song;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SongRepository extends JpaRepository<Song, Integer> {

    List<Song> findByNameContains(String query);
    List<Song> findByAlbum(Album album);
    Page<Song> findAll(Pageable pageable);
    Page<Song> findByNameContains(String query, Pageable pageable);
}
