package com.practice.music.validators;

import com.practice.music.entities.User;
import com.practice.music.models.LoginForm;
import com.practice.music.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import java.util.Optional;

@Component
public class LoginFormValidator implements Validator {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz == LoginForm.class;
    }

    @Override
    public void validate(Object target, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "login", "NotEmpty.appUserForm.userName");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "NotEmpty.appUserForm.password");
        checkLoginAndPasswordInDataBase(target, errors);
    }

    private void checkLoginAndPasswordInDataBase(Object target, Errors errors) {
        LoginForm loginForm = (LoginForm) target;
        Optional<User> userOptional = userRepository.findByLogin(loginForm.getLogin());
        if (!userOptional.isPresent() || !passwordEncoder
                .matches(loginForm.getPassword(), userOptional.get().getPassword())) {
            errors.reject("Incorrect.loginForm.loginPassword");
        }
    }
}
