package com.practice.music.validators;

import com.practice.music.entities.User;
import com.practice.music.models.UserForm;
import com.practice.music.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import java.util.Optional;

@Component
public class UserFormValidator implements Validator {

    @Autowired
    private UserRepository userRepository;


    @Override
    public boolean supports(Class<?> clazz) {
        return clazz == UserForm.class;
    }

    @Override
    public void validate(Object target, Errors errors) {
        UserForm userForm = (UserForm) target;
        ValidationUtils.rejectIfEmptyOrWhitespace(errors,"login", "NotEmpty.appUserForm.userName");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors,"password", "NotEmpty.appUserForm.password");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors,"passwordConfirm", "NotEmpty.appUserForm.confirmPassword");
        checkUserInDatabase(userForm,errors);
        if (!checkPasswordsEquality(userForm)){
            errors.rejectValue("password","Match.appUserForm.confirmPassword");
            errors.rejectValue("passwordConfirm","Match.appUserForm.confirmPassword");
        }
    }

    private void checkUserInDatabase(UserForm userForm, Errors errors) {
        Optional<User> user = userRepository.findByLogin(userForm.getLogin());
        if (user.isPresent()){
            errors.rejectValue("login", "Duplicate.appUserForm.login");
        }
    }

    private boolean checkPasswordsEquality(UserForm userForm) {
        return  userForm.getPassword().equals(userForm.getPasswordConfirm());
    }
}
