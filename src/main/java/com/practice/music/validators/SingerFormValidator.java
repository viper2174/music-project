package com.practice.music.validators;

import com.practice.music.models.SingerForm;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class SingerFormValidator implements Validator {

    private static final int MAXIMUM_DATABASE_RESTRICTION = 255;

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz == SingerForm.class;
    }

    @Override
    public void validate(Object target, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "NotEmpty.appSingerForm.name");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "logo", "NotEmpty.appSingerForm.logo");
        ValidationUtils.rejectIfEmpty(errors,"genres", "NotEmpty.appSingerForm.genres");
        SingerForm singerForm = (SingerForm) target;
        if (singerForm.getLogo().length() > MAXIMUM_DATABASE_RESTRICTION) {
            errors.rejectValue("logo", "Incorrect.SingerForm.logo.size");
        }
    }
}
