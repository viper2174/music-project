package com.practice.music.models;

public class SongForm {

    private String name;
    private int albumId;
    private String link;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAlbumId() {
        return albumId;
    }

    public void setAlbumId(int albumId) {
        this.albumId = albumId;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    @Override
    public String toString() {
        return "SongForm{" +
                "name='" + name + '\'' +
                ", albumId=" + albumId +
                ", link='" + link + '\'' +
                '}';
    }
}
