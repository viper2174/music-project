package com.practice.music.models;

import java.util.Objects;

public class JsonPlaylistResponse {

    private String result;
    private String message;
    private int id;

    public JsonPlaylistResponse(String result, String message, int id) {
        this.result = result;
        this.message = message;
        this.id = id;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        JsonPlaylistResponse that = (JsonPlaylistResponse) o;
        return id == that.id &&
                Objects.equals(result, that.result) &&
                Objects.equals(message, that.message);
    }

    @Override
    public int hashCode() {
        return Objects.hash(result, message, id);
    }
}
