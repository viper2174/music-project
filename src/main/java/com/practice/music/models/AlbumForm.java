package com.practice.music.models;

public class AlbumForm {

    private String name;
    private int musicianId;
    private String poster;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMusicianId() {
        return musicianId;
    }

    public void setMusicianId(int musicianId) {
        this.musicianId = musicianId;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    @Override
    public String toString() {
        return "AlbumForm{" +
                "name='" + name + '\'' +
                ", musicianId=" + musicianId +
                ", poster='" + poster + '\'' +
                '}';
    }
}
