package com.practice.music.models;

import java.util.List;

public class SingerForm {

    private String name;
    private String logo;
    private List<Integer> genres;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public List<Integer> getGenres() {
        return genres;
    }

    public void setGenres(List<Integer> genres) {
        this.genres = genres;
    }

    @Override
    public String toString() {
        return "SingerForm{" +
                "name='" + name + '\'' +
                ", logo='" + logo + '\'' +
                ", genres=" + genres +
                '}';
    }
}
