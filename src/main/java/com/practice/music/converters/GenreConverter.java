package com.practice.music.converters;

import com.practice.music.entities.Genre;
import com.practice.music.services.GenreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class GenreConverter implements Converter<String, Genre> {

    @Autowired
    private GenreService genreService;

    @Override
    public Genre convert(String id) {
        Optional<Genre> genreOptional = genreService.findById(Integer.valueOf(id));
        return genreOptional.orElse(null);
    }
}
